#include <stdio.h>
#include <stdlib.h>
#include <kernel.h>

#include "RubiksCube.h"
#include "InputProcessor.h"
#include "GraphicsProcessor.h"
#include "SoundProcessor.h"

/* User main thread parameters */
extern const char sceUserMainThreadName[]= "rubiks_cube_main_thr";
extern const int sceUserMainThreadPriority= SCE_KERNEL_DEFAULT_PRIORITY_USER;
extern const unsigned intsceUserMainThreadStackSize= SCE_KERNEL_STACK_SIZE_DEFAULT_USER_MAIN;

/* libc parameters */
unsigned int sceLibcHeapSize= 64*1024*1024;

int main()
{
	printf("Hello World!\n");
	RubiksCube cube = RubiksCube();
	InputProcessor inputProcessor = InputProcessor();
	GraphicsProcessor graphicsProcessor = GraphicsProcessor();
	SoundProcessor soundProcessor = SoundProcessor(); // starts playing music right away

	while (true) {
		inputProcessor.processInputs();
		graphicsProcessor.render();
		soundProcessor.processSound();
	}

	printf("Bye World!\n");
	return 0;
}
