#pragma once
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sceerror.h>

#include <gxm.h>
#include <kernel.h>
#include <ctrl.h>
#include <display.h>
#include <libdbg.h>

#include <libdbgfont.h>
#include <math.h>
#include <vectormath.h>

#include "definitions.h"
#include "RubiksCube.h"
#include "InputProcessor.h"

using namespace sce::Vectormath::Simd::Aos;

static int returnCode;

/*	The build process for the sample embeds the shader programs directly into the
	executable using the symbols below.  This is purely for convenience, it is
	equivalent to simply load the binary file into memory and cast the contents
	to type SceGxmProgram.
*/
extern const SceGxmProgram binaryClearVGxpStart;
extern const SceGxmProgram binaryClearFGxpStart;

/*	Data structure for clear geometry */
typedef struct ClearVertex
{
	float x;
	float y;
} ClearVertex;

// !! Data related to rendering vertex.
extern const SceGxmProgram binaryBasicVGxpStart;
extern const SceGxmProgram binaryBasicFGxpStart;

/*	Data structure for basic geometry */
typedef struct BasicVertex
{
	float x;
	float y;
	float z;

	float nx;
	float ny;
	float nz;

	int rotate;

	uint32_t color; // Data gets expanded to float 4 in vertex shader.
} BasicVertex;

/*	Data structure to pass through the display queue.  This structure is
	serialized during sceGxmDisplayQueueAddEntry, and is used to pass
	arbitrary data to the display callback function, called from an internal
	thread once the back buffer is ready to be displayed.

	In this example, we only need to pass the base address of the buffer.
*/
typedef struct DisplayData
{
	void *address;
} DisplayData;

static SceGxmContextParams		s_contextParams;			/* libgxm context parameter */
static SceGxmRenderTargetParams s_renderTargetParams;		/* libgxm render target parameter */
static SceGxmContext			*s_context			= NULL;	/* libgxm context */
static SceGxmRenderTarget		*s_renderTarget		= NULL;	/* libgxm render target */
static SceGxmShaderPatcher		*s_shaderPatcher	= NULL;	/* libgxm shader patcher */

/*	display data */
static void							*s_displayBufferData[ DISPLAY_BUFFER_COUNT ];
static SceGxmSyncObject				*s_displayBufferSync[ DISPLAY_BUFFER_COUNT ];
static int32_t						s_displayBufferUId[ DISPLAY_BUFFER_COUNT ];
static SceGxmColorSurface			s_displaySurface[ DISPLAY_BUFFER_COUNT ];
static uint32_t						s_displayFrontBufferIndex = 0;
static uint32_t						s_displayBackBufferIndex = 0;
static SceGxmDepthStencilSurface	s_depthSurface;

/*	shader data */
static int32_t					s_clearVerticesUId;
static int32_t					s_clearIndicesUId;
static SceGxmShaderPatcherId	s_clearVertexProgramId;
static SceGxmShaderPatcherId	s_clearFragmentProgramId;
static SceGxmShaderPatcherId	s_basicVertexProgramId;
static SceGxmShaderPatcherId	s_basicFragmentProgramId;
static SceUID					s_patcherFragmentUsseUId;
static SceUID					s_patcherVertexUsseUId;
static SceUID					s_patcherBufferUId;
static SceUID					s_depthBufferUId;
static SceUID					s_vdmRingBufferUId;
static SceUID					s_vertexRingBufferUId;
static SceUID					s_fragmentRingBufferUId;
static SceUID					s_fragmentUsseRingBufferUId;
static ClearVertex				*s_clearVertices			= NULL;
static uint16_t					*s_clearIndices				= NULL;
static SceGxmVertexProgram		*s_clearVertexProgram		= NULL;
static SceGxmFragmentProgram	*s_clearFragmentProgram		= NULL;
static SceGxmVertexProgram		*s_basicVertexProgram		= NULL;
static SceGxmFragmentProgram	*s_basicFragmentProgram		= NULL;

static uint16_t					*s_basicIndices[6][RubiksCube::SIZE][RubiksCube::SIZE];
static int32_t					s_basicVerticesUId[6][RubiksCube::SIZE][RubiksCube::SIZE];
static int32_t					s_basicIndiceUId[6][RubiksCube::SIZE][RubiksCube::SIZE];

static const SceGxmProgramParameter *s_wvpParam = NULL;
static const SceGxmProgramParameter *s_mtwParam = NULL;
static const SceGxmProgramParameter *s_rotateParam = NULL;
static const SceGxmProgramParameter *s_eyePositionParam = NULL;

static const int CUBICLEFACE_VERTEX_AMOUNT = 2 * 4;
static const int CUBICLEFACE_INDEX_AMOUNT = 2 * 2 * 3;
static const float CUBICLEFACE_OFFSET = 0.05f;
static const float CUBICLEFACE_ZDELTA = 0.01f;
static const float EPSILON = 0.0001f;
static const float ANIMATION_SPEED = 0.0615f;

static Quat rotationQuaternion = Quat::identity();

class GraphicsProcessor
{
public:
	static Matrix4 s_wvp;
	static Matrix4 s_mtw[6];
	static Matrix4 s_rotate;
	static Vector3 s_eyePosition;
	static BasicVertex *s_basicVertices[6][RubiksCube::SIZE][RubiksCube::SIZE];
	static bool animationInProgress;

	GraphicsProcessor();
	~GraphicsProcessor(void);

	void render();
private:
	static void update();
	static int initGxm( void );
	static void createGxmData( void );
	/*	@brief render libgxm scenes */
	static void renderGxm( void );
	/*	@brief cycle display buffer */
	static void cycleDisplayBuffers( void );
	/*	Callback function for displaying a buffer */
	static void displayCallback( const void *callbackData );
	/*	@brief Destroy scenes with libgxm */
	static void destroyGxmData( void );
	/*	@brief Function to shut down libgxm and the graphics display services
	@return Error code result of processing during execution: <c> SCE_OK </c> on success,
	or another code depending upon the error*/
	static int shutdownGxm( void );
	/* Callback function to allocate memory for the shader patcher */
	static void *patcherHostAlloc( void *userData, uint32_t size );
	/* Callback function to allocate memory for the shader patcher */
	static void patcherHostFree( void *userData, void *mem );
	/*	Helper function to allocate memory and map it for the GPU */
	static void *graphicsAlloc( SceKernelMemBlockType type, uint32_t size, uint32_t alignment, uint32_t attribs, SceUID *uid );
	/*	Helper function to free memory mapped to the GPU */
	static void graphicsFree( SceUID uid );
	/* Helper function to allocate memory and map it as vertex USSE code for the GPU */
	static void *vertexUsseAlloc( uint32_t size, SceUID *uid, uint32_t *usseOffset );
	/* Helper function to free memory mapped as vertex USSE code for the GPU */
	static void vertexUsseFree( SceUID uid );
	/* Helper function to allocate memory and map it as fragment USSE code for the GPU */
	static void *fragmentUsseAlloc( uint32_t size, SceUID *uid, uint32_t *usseOffset );
	/* Helper function to free memory mapped as fragment USSE code for the GPU */
	static void fragmentUsseFree( SceUID uid );
	/*	@brief Initializes the graphics services and the libgxm graphics library
	@return Error code result of processing during execution: <c> SCE_OK </c> on success,
	or another code depending upon the error*/

	static void createCubicleFace(int faceIndex, int x, int y, float offset, float delta, uint32_t color);
	static void createModelToWorld(int faceIndex);
	static void processSwipe(int faceIndex, int x, int y, bool rotateX, Vector4 rotateAxis, float speed);
};

