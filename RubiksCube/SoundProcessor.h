#pragma once

#include <kernel.h>
#include <libdbg.h>
#include <libsysmodule.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <audioout.h>
#include <ngs.h>
#include <ngs/error.h>

#include "definitions.h"
#include "InputProcessor.h"

typedef struct SoundInfo {
	void *pData;
	int  nNumBytes;
	int	 nNumChannels;
	int  nSampleRate;
	int  nNumSamples;
	int  nType;				// SCE_NGS_PLAYER_TYPE_ADPCM or SCE_NGS_PLAYER_TYPE_ADPCM or
							// for ATRAC9, this member holds the configData value
} SoundInfo;

// Variables for audio output (to device and/or file)
static int     s_nAudioOutPortId;
static SceUID  s_uidAudioOutFile;
static int     s_nAudioOutMode;
static SceSize s_uNumBytesPerUpdate;

static SoundInfo        s_music;
static SoundInfo        s_sfx;
static SoundInfo        s_sfx2;

static void             *s_pSysMem;
static SceNgsHSynSystem s_sysHandle;

static void             *s_pRackMemPlayerMusic;
static SceNgsHRack		s_rackPlayerMusic;
static void             *s_pRackMemPlayerSFX;
static SceNgsHRack		s_rackPlayerSFX;
static void             *s_pRackMemPlayerSFX2;
static SceNgsHRack		s_rackPlayerSFX2;
static void             *s_pRackMemMaster;
static SceNgsHRack		s_rackMaster;

static SceNgsHVoice     s_voiceMaster;

static SceNgsHVoice voicePlayerSFX = NULL;
static SceNgsHVoice voicePlayerSFX2 = NULL;


// Variables for audio update thread
static bool   s_bRun       = true;
static int    s_nTickCount = 0;
static SceUID s_threadIdAudioUpdate;

class SoundProcessor
{
public:
	SoundProcessor(void);
	~SoundProcessor(void);

	void processSound();
private:
	static const float DAMPENING = 1.6f;

	static float lastSpeed;

	static int init(void);
	static int initNGS(void);
	static int createRacks( int nChannels, int nChannels2, int nChannels3 );

	static int initPlayer( SceNgsHVoice hVoice, SceUInt32 nModuleId, SoundInfo *pSound, int loopCount );
	static int connectRacks( SceNgsHVoice hVoiceSource, SceNgsHVoice hVoiceDest );
	static int prepareAudioOut( int nMode, int nBufferGranularity, int nSampleRate, const char *strFileName );
	static int startAudioUpdateThread( void );

	static int process(void);
	static int shutdown(void);
	static void shutdownAudioOut( void );

	static int loadWAVFile( const char *strFileName, SoundInfo *pSound );
	static int scanRIFFFileForChunk( const char *strChunkId, SceUID uid, SceUInt32 *puChunkSize );

	static void printParamError( SceNgsHVoice hVoice, SceUInt32 nModuleId );
	static int _AudioUpdateThread( SceSize args, void *argc );
	static int writeAudioOut( const short *pBuffer );
};

