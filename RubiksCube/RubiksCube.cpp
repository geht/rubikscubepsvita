#include "RubiksCube.h"

int RubiksCube::front = 0;
int RubiksCube::back = 1;
int RubiksCube::up = 2;
int RubiksCube::down = 3;
int RubiksCube::left = 4;
int RubiksCube::right = 5;

RubiksCube::Color RubiksCube::cube[6][SIZE][SIZE];

RubiksCube::RubiksCube(void)
{
	init();
}


RubiksCube::~RubiksCube(void)
{
}

void RubiksCube::init(){
	initFace(front, WHITE);
	initFace(back, YELLOW);
	initFace(up, ORANGE);
	initFace(down, RED);
	initFace(left, GREEN);
	initFace(right, BLUE);
}

void RubiksCube::initFace(int faceIndex, Color color) {
	assert (0 <= faceIndex && faceIndex < 6);

	for (int y = 0; y < SIZE; y++) {
		for (int x = 0; x < SIZE; x++) {
			cube[faceIndex][y][x] = color;
		}
	}
}

// turns only the squares on the given face (its) clockwise
void RubiksCube::turnFace(int faceIndex)
{
	assert(0 <= faceIndex && faceIndex < 6);

	Color face[SIZE * SIZE];
	int i = 0;
	for (int y = 0; y < SIZE; y++) {
		for (int x = 0; x < SIZE; x++) {
			face[i] = cube[faceIndex][y][x];
			i++;
		}
	}

	i = 0;
	for (int x = SIZE - 1; x >= 0; x--) {
		for (int y = 0; y < SIZE; y++) {
			cube[faceIndex][y][x] = face[i];
			i++;
		}
	}
}

// turns only the squares on the given face (its) counterclockwise
void RubiksCube::turnFaceP(int faceIndex)
{
	turnFace(faceIndex);
	turnFace(faceIndex);
	turnFace(faceIndex);
}

// turn the front (its) clockwise
void RubiksCube::F()
{
	turnFace(front);
	
	Color u, d, l, r;
	for (int i = 0; i < SIZE; i++) {
		u = cube[up][SIZE - 1][i];
		d = cube[down][0][(SIZE - 1) - i];
		l = cube[left][(SIZE - 1) - i][SIZE - 1];
		r = cube[right][i][0];

		cube[up][SIZE - 1][i] = l;
		cube[down][0][(SIZE - 1) - i] = r;
		cube[left][(SIZE - 1) - i][SIZE - 1] = d;
		cube[right][i][0] = u;
	}
}

// turn the front (its) counterclockwise
void RubiksCube::FP()
{
	F();
	F();
	F();
}

// turn the back (its) clockwise
void RubiksCube::B()
{
	Y();
	Y();
	F();
	Y();
	Y();
}

// turn the back (its) counterclockwise
void RubiksCube::BP()
{
	B();
	B();
	B();
}

// turn the top (its) clockwise
void RubiksCube::U()
{
	XP();
	F();
	X();
}

// turn the top (its) counterclockwise
void RubiksCube::UP()
{
	U();
	U();
	U();
}

// turn the bottom (its) clockwise
void RubiksCube::D()
{
	X();
	F();
	XP();
}

// turn the bottom (its) counterclockwise
void RubiksCube::DP()
{
	D();
	D();
	D();
}

// turn the left (its) clockwise
void RubiksCube::L()
{
	YP();
	F();
	Y();
}

// turn the left (its) counterclockwise
void RubiksCube::LP()
{
	L();
	L();
	L();
}

// turn the right (its) clockwise
void RubiksCube::R()
{
	Y();
	F();
	YP();
}

// turn the right (its) counterclockwise
void RubiksCube::RP()
{
	R();
	R();
	R();
}

// turn the middle layer (between L and R) (Ls) clockwise
void RubiksCube::M()
{
	XP();
	LP();
	R();	
}

// turn the middle layer (between L and R) (Ls) counterclockwise
void RubiksCube::MP()
{
	M();
	M();
	M();
}

// turn the equatorial layer (between U and D) (Ds) clockwise
void RubiksCube::E()
{
	YP();
	DP();
	U();
}

// turn the equatorial layer (between U and D) (Ds) counterclockwise
void RubiksCube::EP()
{
	E();
	E();
	E();
}

// turn the standing layer (between F and B) (Fs) clockwise
void RubiksCube::S()
{
	Z();
	B();
	FP();
}

// turn the standing layer (between F and B) (Fs) counterclockwise
void RubiksCube::SP()
{
	S();
	S();
	S();
}

// turn the two front layers (Fs) clockwise
void RubiksCube::f()
{
	Z();
	B();
}

// turn the two front layers (Fs) counterclockwise
void RubiksCube::fP()
{
	f();
	f();
	f();
}

// turn the two back layers (Bs) clockwise 
void RubiksCube::b()
{
	ZP();
	F();
}

// turn the two back layers (Bs) counterclockwise 
void RubiksCube::bP()
{
	b();
	b();
	b();
}

// turn the two top layers (Us) clockwise
void RubiksCube::u()
{
	Y();
	D();
}

// turn the two top layers (Us) counterclockwise
void RubiksCube::uP()
{
	u();
	u();
	u();
}

// turn the two bottom layers (Ds) clockwise
void RubiksCube::d()
{
	YP();
	U();
}

// turn the two bottom layers (Ds) counterclockwise
void RubiksCube::dP()
{
	d();
	d();
	d();
}

// turn the two left layers (Ls) clockwise
void RubiksCube::l()
{
	XP();
	R();
}

// turn the two left layers (Ls) counterclockwise
void RubiksCube::lP()
{
	l();
	l();
	l();
}

// turn the two right layers (Rs) clockwise
void RubiksCube::r()
{
	X();
	L();
}

// turn the two right layers (Rs) counterclockwise
void RubiksCube::rP()
{
	r();
	r();
	r();
}

// turn the whole cube clockwise around the x-axis
void RubiksCube::X()
{
	int nfront = down;
	int nback = up;
	int nup = front;
	int ndown = back;

	front = nfront;
	back = nback;
	up = nup;
	down = ndown;

	turnFaceP(left);
	turnFace(right);
}

// turn the whole cube counterclockwise around the x-axis
void RubiksCube::XP()
{
	X();
	X();
	X();
}

// turn the whole cube clockwise around the y-axis
void RubiksCube::Y()
{
	turnFace(back);
	turnFace(back);

	int nfront = right;
	int nback = left;
	int nleft = front;
	int nright = back;

	front = nfront;
	back = nback;
	left = nleft;
	right = nright;

	turnFace(back);
	turnFace(back);

	turnFace(up);
	turnFaceP(down);
}

// turn the whole cube counterclockwise around the y-axis
void RubiksCube::YP()
{
	Y();
	Y();
	Y();
}

// turn the whole cube clockwise around the z-axis
void RubiksCube::Z()
{
	XP();
	YP();
	X();
}

// turn the whole cube counterclockwise around the z-axis
void RubiksCube::ZP()
{
	Z();
	Z();
	Z();
}

void RubiksCube::print()
{
	printf("front: %i\n", front);
	printFace(front);
	printf("back: %i\n", back);
	printFace(back);
	printf("up: %i\n", up);
	printFace(up);
	printf("down: %i\n", down);
	printFace(down);
	printf("left: %i\n", left);
	printFace(left);
	printf("right: %i\n", right);
	printFace(right);
	printf("\n");
}

void RubiksCube::printFace(int faceIndex)
{
	assert(0 <= faceIndex && faceIndex < 6);

	for (int y = 0; y < SIZE; y++) {
		for (int x = 0; x < SIZE; x++) {
			printf("%s ", getColorName(cube[faceIndex][y][x]));
		}
		printf("\n");
	}
}

char* RubiksCube::getColorName(Color color) {
	switch(color) {
		case WHITE: return "WHITE";
		case YELLOW: return "YELLOW";
		case ORANGE: return "ORANGE";
		case RED: return "RED";
		case GREEN: return "GREEN";
		case BLUE: return "BLUE";
	}

	return "";
}