#define PI							3.141592653

/*	Define the debug font pixel color format to render to. */
#define DBGFONT_PIXEL_FORMAT		SCE_DBGFONT_PIXELFORMAT_A8B8G8R8

/*	Define the width and height to render at the native resolution */
#define DISPLAY_WIDTH				960
#define DISPLAY_HEIGHT				544
#define DISPLAY_STRIDE_IN_PIXELS	1024

/*	Define the libgxm color format to render to.
	This should be kept in sync with the display format to use with the SceDisplay library.
*/
#define DISPLAY_COLOR_FORMAT		SCE_GXM_COLOR_FORMAT_A8B8G8R8
#define DISPLAY_PIXEL_FORMAT		SCE_DISPLAY_PIXELFORMAT_A8B8G8R8

/*	Define the number of back buffers to use with this sample.  Most applications
	should use a value of 2 (double buffering) or 3 (triple buffering).
*/
#define DISPLAY_BUFFER_COUNT		3

/*	Define the maximum number of queued swaps that the display queue will allow.
	This limits the number of frames that the CPU can get ahead of the GPU,
	and is independent of the actual number of back buffers.  The display
	queue will block during sceGxmDisplayQueueAddEntry if this number of swaps
	have already been queued.
*/
#define DISPLAY_MAX_PENDING_SWAPS	2

/*	Helper macro to align a value */
#define ALIGN(x, a)					(((x) + ((a) - 1)) & ~((a) - 1))

#define INPUT_FILE_NAME		("app0:wave/music.wav")
#define INPUT_FILE_NAME2	("app0:wave/crck2.wav")
#define INPUT_FILE_NAME3	("app0:wave/crck.wav")
#define OUTPUT_MODE			(NGS_SEND_TO_DEVICE)
#define OUTPUT_FILE_NAME	("app0:out_pcm_player.raw")

// NUM_FRAMES defines how long to run the sample for
#define NUM_FRAMES		(1200)

// NUM_MODULES defines how many unique module types NGS will allow to be loaded
// this number refers to the number of module types, regardless of the number of instances
#define NUM_MODULES		(14)

// SYS_GRANULARITY defines how many samples are produced each update
#define SYS_GRANULARITY	(512)

// SYS_SAMPLE_RATE defines the sample rate for NGS
#define SYS_SAMPLE_RATE	(48000)

// Number of microseconds sleep in main processing loop
#define SLEEP_PERIOD	(1000 * 4)

#define AUDIO_UPDATE_THREAD_STACK_SIZE	(8 * 1024)

#define FILE_READ_CHUNK_SIZE	(64 * 1024)

// Flags for audio output mode
#define NGS_NO_OUTPUT		(0x00)
#define NGS_SEND_TO_DEVICE	(0x01)
#define NGS_WRITE_TO_FILE	(0x02)

// Flags for audio output config mode
#define NGS_SET_VOLUME      (0x00)