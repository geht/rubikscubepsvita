#include "GraphicsProcessor.h"

Matrix4 GraphicsProcessor::s_wvp;
Matrix4 GraphicsProcessor::s_mtw[6];
Matrix4 GraphicsProcessor::s_rotate;
Vector3 GraphicsProcessor::s_eyePosition;
BasicVertex* GraphicsProcessor::s_basicVertices[6][RubiksCube::SIZE][RubiksCube::SIZE];
bool GraphicsProcessor::animationInProgress;

GraphicsProcessor::GraphicsProcessor()
{
	returnCode = SCE_OK;

	/* initialize libdbgfont and libgxm */
	returnCode = initGxm();
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

    SceDbgFontConfig config;
	memset( &config, 0, sizeof(SceDbgFontConfig) );
	config.fontSize = SCE_DBGFONT_FONTSIZE_LARGE;

	returnCode = sceDbgFontInit( &config );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	/* Message for SDK sample auto test */
	printf( "## simple: INIT SUCCEEDED ##\n" );
	
	/* create gxm graphics data */
	createGxmData();
}

GraphicsProcessor::~GraphicsProcessor(void)
{
	// wait until rendering is done 
	sceGxmFinish( s_context );
	// destroy gxm graphics data 
	destroyGxmData();
	// shutdown libdbgfont and libgxm 
	returnCode = shutdownGxm();
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );
	// Message for SDK sample auto test
	printf( "## api_libdbgfont/simple: FINISHED ##\n" );
}

int GraphicsProcessor::initGxm( void )
{
	/* ---------------------------------------------------------------------
	Initialize libgxm

	First we must initialize the libgxm library by calling sceGxmInitialize.
	The single argument to this function is the size of the parameter buffer to
	allocate for the GPU.  We will use the default 16MiB here.

	Once initialized, we need to create a rendering context to allow to us
	to render scenes on the GPU.  We use the default initialization
	parameters here to set the sizes of the various context ring buffers.

	Finally we create a render target to describe the geometry of the back
	buffers we will render to.  This object is used purely to schedule
	rendering jobs for the given dimensions, the color surface and
	depth/stencil surface must be allocated separately.
	--------------------------------------------------------------------- */

	int returnCode = SCE_OK;

	/* set up parameters */
	SceGxmInitializeParams initializeParams;
	memset( &initializeParams, 0, sizeof(SceGxmInitializeParams) );
	initializeParams.flags = 0;
	initializeParams.displayQueueMaxPendingCount = DISPLAY_MAX_PENDING_SWAPS;
	initializeParams.displayQueueCallback = displayCallback;
	initializeParams.displayQueueCallbackDataSize = sizeof(DisplayData);
	initializeParams.parameterBufferSize = SCE_GXM_DEFAULT_PARAMETER_BUFFER_SIZE;

	/* start libgxm */
	returnCode = sceGxmInitialize( &initializeParams );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	/* allocate ring buffer memory using default sizes */
	void *vdmRingBuffer = graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, SCE_GXM_DEFAULT_VDM_RING_BUFFER_SIZE, 4, SCE_GXM_MEMORY_ATTRIB_READ, &s_vdmRingBufferUId );

	void *vertexRingBuffer = graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, SCE_GXM_DEFAULT_VERTEX_RING_BUFFER_SIZE, 4, SCE_GXM_MEMORY_ATTRIB_READ, &s_vertexRingBufferUId );

	void *fragmentRingBuffer = graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, SCE_GXM_DEFAULT_FRAGMENT_RING_BUFFER_SIZE, 4, SCE_GXM_MEMORY_ATTRIB_READ, &s_fragmentRingBufferUId );

	uint32_t fragmentUsseRingBufferOffset;
	void *fragmentUsseRingBuffer = fragmentUsseAlloc( SCE_GXM_DEFAULT_FRAGMENT_USSE_RING_BUFFER_SIZE, &s_fragmentUsseRingBufferUId, &fragmentUsseRingBufferOffset );

	/* create a rendering context */
	memset( &s_contextParams, 0, sizeof(SceGxmContextParams) );
	s_contextParams.hostMem = malloc( SCE_GXM_MINIMUM_CONTEXT_HOST_MEM_SIZE );
	s_contextParams.hostMemSize = SCE_GXM_MINIMUM_CONTEXT_HOST_MEM_SIZE;
	s_contextParams.vdmRingBufferMem = vdmRingBuffer;
	s_contextParams.vdmRingBufferMemSize = SCE_GXM_DEFAULT_VDM_RING_BUFFER_SIZE;
	s_contextParams.vertexRingBufferMem = vertexRingBuffer;
	s_contextParams.vertexRingBufferMemSize = SCE_GXM_DEFAULT_VERTEX_RING_BUFFER_SIZE;
	s_contextParams.fragmentRingBufferMem = fragmentRingBuffer;
	s_contextParams.fragmentRingBufferMemSize = SCE_GXM_DEFAULT_FRAGMENT_RING_BUFFER_SIZE;
	s_contextParams.fragmentUsseRingBufferMem = fragmentUsseRingBuffer;
	s_contextParams.fragmentUsseRingBufferMemSize = SCE_GXM_DEFAULT_FRAGMENT_USSE_RING_BUFFER_SIZE;
	s_contextParams.fragmentUsseRingBufferOffset = fragmentUsseRingBufferOffset;
	returnCode = sceGxmCreateContext( &s_contextParams, &s_context );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	/* set buffer sizes for this sample */
	const uint32_t patcherBufferSize = 64*1024;
	const uint32_t patcherVertexUsseSize = 64*1024;
	const uint32_t patcherFragmentUsseSize = 64*1024;

	/* allocate memory for buffers and USSE code */
	void *patcherBuffer = graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, patcherBufferSize, 4, SCE_GXM_MEMORY_ATTRIB_WRITE|SCE_GXM_MEMORY_ATTRIB_WRITE, &s_patcherBufferUId );

	uint32_t patcherVertexUsseOffset;
	void *patcherVertexUsse = vertexUsseAlloc( patcherVertexUsseSize, &s_patcherVertexUsseUId, &patcherVertexUsseOffset );

	uint32_t patcherFragmentUsseOffset;
	void *patcherFragmentUsse = fragmentUsseAlloc( patcherFragmentUsseSize, &s_patcherFragmentUsseUId, &patcherFragmentUsseOffset );

	/* create a shader patcher */
	SceGxmShaderPatcherParams patcherParams;
	memset( &patcherParams, 0, sizeof(SceGxmShaderPatcherParams) );
	patcherParams.userData = NULL;
	patcherParams.hostAllocCallback = &patcherHostAlloc;
	patcherParams.hostFreeCallback = &patcherHostFree;
	patcherParams.bufferAllocCallback = NULL;
	patcherParams.bufferFreeCallback = NULL;
	patcherParams.bufferMem = patcherBuffer;
	patcherParams.bufferMemSize = patcherBufferSize;
	patcherParams.vertexUsseAllocCallback = NULL;
	patcherParams.vertexUsseFreeCallback = NULL;
	patcherParams.vertexUsseMem = patcherVertexUsse;
	patcherParams.vertexUsseMemSize = patcherVertexUsseSize;
	patcherParams.vertexUsseOffset = patcherVertexUsseOffset;
	patcherParams.fragmentUsseAllocCallback = NULL;
	patcherParams.fragmentUsseFreeCallback = NULL;
	patcherParams.fragmentUsseMem = patcherFragmentUsse;
	patcherParams.fragmentUsseMemSize = patcherFragmentUsseSize;
	patcherParams.fragmentUsseOffset = patcherFragmentUsseOffset;
	returnCode = sceGxmShaderPatcherCreate( &patcherParams, &s_shaderPatcher );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	/* create a render target */
	memset( &s_renderTargetParams, 0, sizeof(SceGxmRenderTargetParams) );
	s_renderTargetParams.flags = 0;
	s_renderTargetParams.width = DISPLAY_WIDTH;
	s_renderTargetParams.height = DISPLAY_HEIGHT;
	s_renderTargetParams.scenesPerFrame = 1;
	s_renderTargetParams.multisampleMode = SCE_GXM_MULTISAMPLE_NONE;
	s_renderTargetParams.multisampleLocations	= 0;
	s_renderTargetParams.driverMemBlock = SCE_UID_INVALID_UID;

	returnCode = sceGxmCreateRenderTarget( &s_renderTargetParams, &s_renderTarget );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );


/* ---------------------------------------------------------------------
	Allocate display buffers, set up the display queue

	We will allocate our back buffers in CDRAM, and create a color
	surface for each of them.

	To allow display operations done by the CPU to be synchronized with
	rendering done by the GPU, we also create a SceGxmSyncObject for each
	display buffer.  This sync object will be used with each scene that
	renders to that buffer and when queueing display flips that involve
	that buffer (either flipping from or to).

	Finally we create a display queue object that points to our callback
	function.
	--------------------------------------------------------------------- */

	/* allocate memory and sync objects for display buffers */
	for ( unsigned int i = 0 ; i < DISPLAY_BUFFER_COUNT ; ++i )
	{
		/* allocate memory with large size to ensure physical contiguity */
		s_displayBufferData[i] = graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RWDATA, ALIGN(4*DISPLAY_STRIDE_IN_PIXELS*DISPLAY_HEIGHT, 1*1024*1024), SCE_GXM_COLOR_SURFACE_ALIGNMENT, SCE_GXM_MEMORY_ATTRIB_READ|SCE_GXM_MEMORY_ATTRIB_WRITE, &s_displayBufferUId[i] );
		SCE_DBG_ALWAYS_ASSERT( s_displayBufferData[i] );

		/* memset the buffer to debug color */
		for ( unsigned int y = 0 ; y < DISPLAY_HEIGHT ; ++y )
		{
			unsigned int *row = (unsigned int *)s_displayBufferData[i] + y*DISPLAY_STRIDE_IN_PIXELS;

			for ( unsigned int x = 0 ; x < DISPLAY_WIDTH ; ++x )
			{
				row[x] = 0x0;
			}
		}

		/* initialize a color surface for this display buffer */
		returnCode = sceGxmColorSurfaceInit( &s_displaySurface[i], DISPLAY_COLOR_FORMAT, SCE_GXM_COLOR_SURFACE_LINEAR, SCE_GXM_COLOR_SURFACE_SCALE_NONE,
											 SCE_GXM_OUTPUT_REGISTER_SIZE_32BIT, DISPLAY_WIDTH, DISPLAY_HEIGHT, DISPLAY_STRIDE_IN_PIXELS, s_displayBufferData[i] );
		SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

		/* create a sync object that we will associate with this buffer */
		returnCode = sceGxmSyncObjectCreate( &s_displayBufferSync[i] );
		SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );
	}

	/* compute the memory footprint of the depth buffer */
	const uint32_t alignedWidth = ALIGN( DISPLAY_WIDTH, SCE_GXM_TILE_SIZEX );
	const uint32_t alignedHeight = ALIGN( DISPLAY_HEIGHT, SCE_GXM_TILE_SIZEY );
	uint32_t sampleCount = alignedWidth*alignedHeight;
	uint32_t depthStrideInSamples = alignedWidth;

	/* allocate it */
	void *depthBufferData = graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, 4*sampleCount, SCE_GXM_DEPTHSTENCIL_SURFACE_ALIGNMENT, SCE_GXM_MEMORY_ATTRIB_READ|SCE_GXM_MEMORY_ATTRIB_WRITE, &s_depthBufferUId );

	/* create the SceGxmDepthStencilSurface structure */
	returnCode = sceGxmDepthStencilSurfaceInit( &s_depthSurface, SCE_GXM_DEPTH_STENCIL_FORMAT_S8D24, SCE_GXM_DEPTH_STENCIL_SURFACE_TILED, depthStrideInSamples, depthBufferData, NULL );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	// sceGxmSetCullMode( s_context, SCE_GXM_CULL_CW );

	return returnCode;
}

void GraphicsProcessor::render()
{
	/* render libgxm scenes */
	update();
	renderGxm();
	sceDbgFontPrint( 20, 20, 0xffffffff, "Hello World" );
	cycleDisplayBuffers();
}

void GraphicsProcessor::update()
{
	Quat rotationVelocity(InputProcessor::turningAngleY, -InputProcessor::turningAngleX, InputProcessor::turningAngleZ, 0.0f);
	rotationQuaternion += 0.5f * rotationVelocity * rotationQuaternion;
	rotationQuaternion = normalize(rotationQuaternion);

	Matrix4 translation = Matrix4::translation(Vector3(-1.5f, -1.5f, -1.5f));
	Matrix4 rotation = Matrix4::rotation(rotationQuaternion);
    Matrix4 lookAt = Matrix4::lookAt(Point3(0.0f, 0.0f, -6.5f * InputProcessor::accumulatedScale), Point3(0.0f, 0.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f));
    Matrix4 perspective = Matrix4::perspective(3.141592f / 4.0f, (float)DISPLAY_WIDTH/(float)DISPLAY_HEIGHT, 0.1f, 20.0f);
    s_wvp = perspective * lookAt;
	s_eyePosition = Vector3(0.0f, 0.0f, -6.5f * InputProcessor::accumulatedScale);

	for (int i = 0; i < 6; i++)
	{
		for (int y = 0; y < RubiksCube::SIZE; y++)
		{
			for (int x = 0; x < RubiksCube::SIZE; x++)
			{
				createCubicleFace(i, x, y, CUBICLEFACE_OFFSET, CUBICLEFACE_ZDELTA, RubiksCube::cube[i][y][x]);
			}
		}

		createModelToWorld(i);
		s_mtw[i] = rotation * translation * s_mtw[i];
	}

	switch(InputProcessor::state)
	{
		case InputProcessor::NO_MOVEMENT:
			animationInProgress = false;
			break;
		case InputProcessor::SWIPING:
			processSwipe(InputProcessor::hitFaceIndex, InputProcessor::hitIndexX, InputProcessor::hitIndexY, InputProcessor::rotateX, InputProcessor::rotateAxis, InputProcessor::swipeSpeed);
			break;
		case InputProcessor::SNAPPING:
			processSwipe(InputProcessor::hitFaceIndex, InputProcessor::hitIndexX, InputProcessor::hitIndexY, InputProcessor::rotateX, InputProcessor::rotateAxis, InputProcessor::swipeSpeed);

			if (!animationInProgress)
			{
				animationInProgress = true;

				if (abs((InputProcessor::swipeSpeed)) < PI / 4.0f)
					InputProcessor::swipe = false;
				
				else
					InputProcessor::swipe = true;
			}
			else
			{
				if (!InputProcessor::swipe)
				{
					if (InputProcessor::swipeSpeed < 0.0f)
						InputProcessor::swipeSpeed += ANIMATION_SPEED;
					else if (InputProcessor::swipeSpeed > 0.0f)
						InputProcessor::swipeSpeed -= ANIMATION_SPEED;

					if (abs(InputProcessor::swipeSpeed) - ANIMATION_SPEED <= 0.0f)
						animationInProgress = false;
				}
				else
				{
					if (InputProcessor::swipeSpeed < 0.0f && InputProcessor::swipeSpeed < -PI / 2.0f || InputProcessor::swipeSpeed > 0.0f && InputProcessor::swipeSpeed < PI / 2.0f)
						InputProcessor::swipeSpeed += ANIMATION_SPEED;
					else
						InputProcessor::swipeSpeed -= ANIMATION_SPEED;

					if (abs(InputProcessor::swipeSpeed) + ANIMATION_SPEED >= PI / 2.0f && abs(InputProcessor::swipeSpeed) - ANIMATION_SPEED <= PI / 2.0f)
						animationInProgress = false;
				}
			}
			break;
	}
}

void GraphicsProcessor::createGxmData( void )
{
	/* ---------------------------------------------------------------------
	Create a shader patcher and register programs

	A shader patcher object is required to produce vertex and fragment
	programs from the shader compiler output.  First we create a shader
	patcher instance, using callback functions to allow it to allocate
	and free host memory for internal state.

	In order to create vertex and fragment programs for a particular
	shader, the compiler output must first be registered to obtain an ID
	for that shader.  Within a single ID, vertex and fragment programs
	are reference counted and could be shared if created with identical
	parameters.  To maximise this sharing, programs should only be
	registered with the shader patcher once if possible, so we will do
	this now.
	--------------------------------------------------------------------- */

	/* register programs with the patcher */
	int returnCode = sceGxmShaderPatcherRegisterProgram( s_shaderPatcher, &binaryClearVGxpStart, &s_clearVertexProgramId );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );
	returnCode = sceGxmShaderPatcherRegisterProgram( s_shaderPatcher, &binaryClearFGxpStart, &s_clearFragmentProgramId );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

    returnCode = sceGxmShaderPatcherRegisterProgram( s_shaderPatcher, &binaryBasicVGxpStart, &s_basicVertexProgramId );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );
	returnCode = sceGxmShaderPatcherRegisterProgram( s_shaderPatcher, &binaryBasicFGxpStart, &s_basicFragmentProgramId );
    SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

/* ---------------------------------------------------------------------
	Create the programs and data for the clear

	On SGX hardware, vertex programs must perform the unpack operations
	on vertex data, so we must define our vertex formats in order to
	create the vertex program.  Similarly, fragment programs must be
	specialized based on how they output their pixels and MSAA mode
	(and texture format on ES1).

	We define the clear geometry vertex format here and create the vertex
	and fragment program.

	The clear vertex and index data is static, we allocate and write the
	data here.
	--------------------------------------------------------------------- */

	/* get attributes by name to create vertex format bindings */
	const SceGxmProgram *clearProgram = sceGxmShaderPatcherGetProgramFromId( s_clearVertexProgramId );
	SCE_DBG_ALWAYS_ASSERT( clearProgram );
	const SceGxmProgramParameter *paramClearPositionAttribute = sceGxmProgramFindParameterByName( clearProgram, "aPosition" );
	SCE_DBG_ALWAYS_ASSERT( paramClearPositionAttribute && ( sceGxmProgramParameterGetCategory(paramClearPositionAttribute) == SCE_GXM_PARAMETER_CATEGORY_ATTRIBUTE ) );

	/* create clear vertex format */
	SceGxmVertexAttribute clearVertexAttributes[1];
	SceGxmVertexStream clearVertexStreams[1];
	clearVertexAttributes[0].streamIndex = 0;
	clearVertexAttributes[0].offset = 0;
	clearVertexAttributes[0].format = SCE_GXM_ATTRIBUTE_FORMAT_F32;
	clearVertexAttributes[0].componentCount = 2;
	clearVertexAttributes[0].regIndex = sceGxmProgramParameterGetResourceIndex( paramClearPositionAttribute );
	clearVertexStreams[0].stride = sizeof(ClearVertex);
	clearVertexStreams[0].indexSource = SCE_GXM_INDEX_SOURCE_INDEX_16BIT;

	/* create clear programs */
	returnCode = sceGxmShaderPatcherCreateVertexProgram( s_shaderPatcher, s_clearVertexProgramId, clearVertexAttributes, 1, clearVertexStreams, 1, &s_clearVertexProgram );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	returnCode = sceGxmShaderPatcherCreateFragmentProgram( s_shaderPatcher, s_clearFragmentProgramId,
														   SCE_GXM_OUTPUT_REGISTER_FORMAT_UCHAR4, SCE_GXM_MULTISAMPLE_NONE, NULL,
														   sceGxmShaderPatcherGetProgramFromId(s_clearVertexProgramId), &s_clearFragmentProgram );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	/* create the clear triangle vertex/index data */
	s_clearVertices = (ClearVertex *)graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, 3*sizeof(ClearVertex), 4, SCE_GXM_MEMORY_ATTRIB_READ, &s_clearVerticesUId );
	s_clearIndices = (uint16_t *)graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, 3*sizeof(uint16_t), 2, SCE_GXM_MEMORY_ATTRIB_READ, &s_clearIndicesUId );

	s_clearVertices[0].x = -1.0f;
	s_clearVertices[0].y = -1.0f;
	s_clearVertices[1].x =  3.0f;
	s_clearVertices[1].y = -1.0f;
	s_clearVertices[2].x = -1.0f;
	s_clearVertices[2].y =  3.0f;

	s_clearIndices[0] = 0;
	s_clearIndices[1] = 1;
	s_clearIndices[2] = 2;

    /* get attributes by name to create vertex format bindings */
	/* first retrieve the underlying program to extract binding information */
	const SceGxmProgram *basicProgram = sceGxmShaderPatcherGetProgramFromId( s_basicVertexProgramId );
	SCE_DBG_ALWAYS_ASSERT( basicProgram );
	const SceGxmProgramParameter *paramBasicPositionAttribute = sceGxmProgramFindParameterByName( basicProgram, "aPosition" );
	SCE_DBG_ALWAYS_ASSERT( paramBasicPositionAttribute && ( sceGxmProgramParameterGetCategory(paramBasicPositionAttribute) == SCE_GXM_PARAMETER_CATEGORY_ATTRIBUTE ) );
	const SceGxmProgramParameter *paramBasicNormalAttribute = sceGxmProgramFindParameterByName( basicProgram, "aNormal" );
	SCE_DBG_ALWAYS_ASSERT( paramBasicNormalAttribute && ( sceGxmProgramParameterGetCategory(paramBasicPositionAttribute) == SCE_GXM_PARAMETER_CATEGORY_ATTRIBUTE ) );
	const SceGxmProgramParameter *paramBasicRotateAttribute = sceGxmProgramFindParameterByName( basicProgram, "aRotate" );
	SCE_DBG_ALWAYS_ASSERT( paramBasicRotateAttribute && ( sceGxmProgramParameterGetCategory(paramBasicRotateAttribute) == SCE_GXM_PARAMETER_CATEGORY_ATTRIBUTE ) );
	const SceGxmProgramParameter *paramBasicColorAttribute = sceGxmProgramFindParameterByName( basicProgram, "aColor" );
	SCE_DBG_ALWAYS_ASSERT( paramBasicColorAttribute && ( sceGxmProgramParameterGetCategory(paramBasicColorAttribute) == SCE_GXM_PARAMETER_CATEGORY_ATTRIBUTE ) );


	/* create shaded triangle vertex format */
	SceGxmVertexAttribute basicVertexAttributes[4];
	SceGxmVertexStream basicVertexStreams[1];
	basicVertexAttributes[0].streamIndex = 0;
	basicVertexAttributes[0].offset = 0;
	basicVertexAttributes[0].format = SCE_GXM_ATTRIBUTE_FORMAT_F32;
	basicVertexAttributes[0].componentCount = 3;
	basicVertexAttributes[0].regIndex = sceGxmProgramParameterGetResourceIndex( paramBasicPositionAttribute );
	basicVertexAttributes[1].streamIndex = 0;
	basicVertexAttributes[1].offset = 12;
	basicVertexAttributes[1].format = SCE_GXM_ATTRIBUTE_FORMAT_F32;
	basicVertexAttributes[1].componentCount = 3;
	basicVertexAttributes[1].regIndex = sceGxmProgramParameterGetResourceIndex( paramBasicNormalAttribute );
	basicVertexAttributes[2].streamIndex = 0;
	basicVertexAttributes[2].offset = 24;
	basicVertexAttributes[2].format = SCE_GXM_ATTRIBUTE_FORMAT_S8;
	basicVertexAttributes[2].componentCount = 1;
	basicVertexAttributes[2].regIndex = sceGxmProgramParameterGetResourceIndex( paramBasicRotateAttribute );
	basicVertexAttributes[3].streamIndex = 0;
	basicVertexAttributes[3].offset = 28;
	basicVertexAttributes[3].format = SCE_GXM_ATTRIBUTE_FORMAT_U8N; // Mapping relation clarified.
	basicVertexAttributes[3].componentCount = 4;
	basicVertexAttributes[3].regIndex = sceGxmProgramParameterGetResourceIndex( paramBasicColorAttribute );
	basicVertexStreams[0].stride = sizeof(BasicVertex);
	basicVertexStreams[0].indexSource = SCE_GXM_INDEX_SOURCE_INDEX_16BIT;

	/* create shaded triangle shaders */
	returnCode = sceGxmShaderPatcherCreateVertexProgram( s_shaderPatcher, s_basicVertexProgramId, basicVertexAttributes, 4,
														 basicVertexStreams, 1, &s_basicVertexProgram );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	returnCode = sceGxmShaderPatcherCreateFragmentProgram( s_shaderPatcher, s_basicFragmentProgramId,
														   SCE_GXM_OUTPUT_REGISTER_FORMAT_UCHAR4, SCE_GXM_MULTISAMPLE_NONE, NULL,
														   sceGxmShaderPatcherGetProgramFromId(s_basicVertexProgramId), &s_basicFragmentProgram );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	/* find vertex uniforms by name and cache parameter information */
	s_wvpParam = sceGxmProgramFindParameterByName( basicProgram, "worldViewProj" );
	SCE_DBG_ALWAYS_ASSERT( s_wvpParam && ( sceGxmProgramParameterGetCategory( s_wvpParam ) == SCE_GXM_PARAMETER_CATEGORY_UNIFORM ) );

	/* find vertex uniforms by name and cache parameter information */
	s_mtwParam = sceGxmProgramFindParameterByName( basicProgram, "modelToWorld" );
	SCE_DBG_ALWAYS_ASSERT( s_mtwParam && ( sceGxmProgramParameterGetCategory( s_mtwParam ) == SCE_GXM_PARAMETER_CATEGORY_UNIFORM ) );

	/* find vertex uniforms by name and cache parameter information */
	s_rotateParam = sceGxmProgramFindParameterByName( basicProgram, "rotation" );
	SCE_DBG_ALWAYS_ASSERT( s_rotateParam && ( sceGxmProgramParameterGetCategory( s_rotateParam ) == SCE_GXM_PARAMETER_CATEGORY_UNIFORM ) );

	const SceGxmProgram *basicFragmentProgram = sceGxmShaderPatcherGetProgramFromId( s_basicFragmentProgramId );
	SCE_DBG_ALWAYS_ASSERT( basicFragmentProgram );

	/* find vertex uniforms by name and cache parameter information */
	s_eyePositionParam = sceGxmProgramFindParameterByName ( basicFragmentProgram, "eyePosition" );
	SCE_DBG_ALWAYS_ASSERT( s_eyePositionParam && ( sceGxmProgramParameterGetCategory( s_eyePositionParam ) == SCE_GXM_PARAMETER_CATEGORY_UNIFORM ) );

	for (int i = 0; i < 6; i++)
	{
		for (int y = 0; y < RubiksCube::SIZE; y++)
		{
			for (int x = 0; x < RubiksCube::SIZE; x++)
			{
				s_basicVertices[i][y][x] = (BasicVertex *)graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, CUBICLEFACE_VERTEX_AMOUNT * sizeof(BasicVertex), 8, SCE_GXM_MEMORY_ATTRIB_READ, &s_basicVerticesUId[i][y][x] );
				s_basicIndices[i][y][x] = (uint16_t *)graphicsAlloc( SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, CUBICLEFACE_INDEX_AMOUNT * sizeof(uint16_t), 2, SCE_GXM_MEMORY_ATTRIB_READ, &s_basicIndiceUId[i][y][x] );
			}
		}
	}
}

void GraphicsProcessor::renderGxm( void )
{
	/* -----------------------------------------------------------------
	Rendering step

	This sample renders a single scene containing the clear triangle.
	Before any drawing can take place, a scene must be started.
	We render to the back buffer, so it is also important to use a
	sync object to ensure that these rendering operations are
	synchronized with display operations.

	The clear triangle shaders do not declare any uniform variables,
	so this may be rendered immediately after setting the vertex and
	fragment program.

	Once clear triangle have been drawn the scene can be ended, which
	submits it for rendering on the GPU.
	----------------------------------------------------------------- */

	/* start rendering to the render target */
	sceGxmBeginScene( s_context, 0, s_renderTarget, NULL, NULL, s_displayBufferSync[s_displayBackBufferIndex], &s_displaySurface[s_displayBackBufferIndex], &s_depthSurface );

	/* set clear shaders */
	sceGxmSetVertexProgram( s_context, s_clearVertexProgram );
	sceGxmSetFragmentProgram( s_context, s_clearFragmentProgram );

	/* draw ther clear triangle */
	sceGxmSetVertexStream( s_context, 0, s_clearVertices );
	sceGxmDraw( s_context, SCE_GXM_PRIMITIVE_TRIANGLES, SCE_GXM_INDEX_FORMAT_U16, s_clearIndices, 3 );

	/* set basic shaders */
	// for now same shader for each cube face
	sceGxmSetVertexProgram( s_context, s_basicVertexProgram );
	sceGxmSetFragmentProgram( s_context, s_basicFragmentProgram );

	/* set the vertex program constants */
	void *vertexDefaultBuffer;
	void *fragmentDefaultBuffer;
	for (int i = 0; i < 6; i++)
	{
		sceGxmReserveVertexDefaultUniformBuffer( s_context, &vertexDefaultBuffer );
		sceGxmSetUniformDataF( vertexDefaultBuffer, s_wvpParam, 0, 16, (float*)&s_wvp );
		sceGxmSetUniformDataF( vertexDefaultBuffer, s_mtwParam, 0, 16, (float*)&s_mtw[i] );
		sceGxmSetUniformDataF( vertexDefaultBuffer, s_rotateParam, 0, 16, (float*)&s_rotate );

		sceGxmReserveFragmentDefaultUniformBuffer ( s_context, &fragmentDefaultBuffer );
		sceGxmSetUniformDataF( fragmentDefaultBuffer, s_eyePositionParam, 0, 3, (float *)&s_eyePosition);

		for (int y = 0; y < RubiksCube::SIZE; y++)
		{
			for (int x = 0; x < RubiksCube::SIZE; x++)
			{
				sceGxmSetVertexStream( s_context, 0, s_basicVertices[i][y][x] );
				sceGxmDraw( s_context, SCE_GXM_PRIMITIVE_TRIANGLES, SCE_GXM_INDEX_FORMAT_U16, s_basicIndices[i][y][x], CUBICLEFACE_INDEX_AMOUNT );
			}
		}
	}

	/* stop rendering to the render target */
	sceGxmEndScene( s_context, NULL, NULL );
}

void GraphicsProcessor::cycleDisplayBuffers( void )
{
	/* -----------------------------------------------------------------
	Flip operation

	Now we have finished submitting rendering work for this frame it
	is time to submit a flip operation.  As part of specifying this
	flip operation we must provide the sync objects for both the old
	buffer and the new buffer.  This is to allow synchronization both
	ways: to not flip until rendering is complete, but also to ensure
	that future rendering to these buffers does not start until the
	flip operation is complete.

	Once we have queued our flip, we manually cycle through our back
	buffers before starting the next frame.
	----------------------------------------------------------------- */

	/* PA heartbeat to notify end of frame */
	sceGxmPadHeartbeat( &s_displaySurface[s_displayBackBufferIndex], s_displayBufferSync[s_displayBackBufferIndex] );

	/* queue the display swap for this frame */
	DisplayData displayData;
	displayData.address = s_displayBufferData[s_displayBackBufferIndex];

	/* front buffer is OLD buffer, back buffer is NEW buffer */
	sceGxmDisplayQueueAddEntry( s_displayBufferSync[s_displayFrontBufferIndex], s_displayBufferSync[s_displayBackBufferIndex], &displayData );

	/* update buffer indices */
	s_displayFrontBufferIndex = s_displayBackBufferIndex;
	s_displayBackBufferIndex = (s_displayBackBufferIndex + 1) % DISPLAY_BUFFER_COUNT;
}

void GraphicsProcessor::displayCallback( const void *callbackData )
{
	/* -----------------------------------------------------------------
	Flip operation

	The callback function will be called from an internal thread once
	queued GPU operations involving the sync objects is complete.
	Assuming we have not reached our maximum number of queued frames,
	this function returns immediately.
	----------------------------------------------------------------- */

	SceDisplayFrameBuf framebuf;

	/* cast the parameters back */
	const DisplayData *displayData = (const DisplayData *)callbackData;


    // Render debug text.
    /* set framebuffer info */
	SceDbgFontFrameBufInfo info;
	memset( &info, 0, sizeof(SceDbgFontFrameBufInfo) );
	info.frameBufAddr = (SceUChar8 *)displayData->address;
	info.frameBufPitch = DISPLAY_STRIDE_IN_PIXELS;
	info.frameBufWidth = DISPLAY_WIDTH;
	info.frameBufHeight = DISPLAY_HEIGHT;
	info.frameBufPixelformat = DBGFONT_PIXEL_FORMAT;

	/* flush font buffer */
	int returnCode = sceDbgFontFlush( &info );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );
	

	/* wwap to the new buffer on the next VSYNC */
	memset(&framebuf, 0x00, sizeof(SceDisplayFrameBuf));
	framebuf.size        = sizeof(SceDisplayFrameBuf);
	framebuf.base        = displayData->address;
	framebuf.pitch       = DISPLAY_STRIDE_IN_PIXELS;
	framebuf.pixelformat = DISPLAY_PIXEL_FORMAT;
	framebuf.width       = DISPLAY_WIDTH;
	framebuf.height      = DISPLAY_HEIGHT;
	returnCode = sceDisplaySetFrameBuf( &framebuf, SCE_DISPLAY_UPDATETIMING_NEXTVSYNC );
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );

	/* block this callback until the swap has occurred and the old buffer is no longer displayed */
	returnCode = sceDisplayWaitVblankStart();
	SCE_DBG_ALWAYS_ASSERT( returnCode == SCE_OK );
}

void GraphicsProcessor::destroyGxmData( void )
{
	/* ---------------------------------------------------------------------
	Destroy the programs and data for the clear and spinning triangle

	Once the GPU is finished, we release all our programs.
	--------------------------------------------------------------------- */

	/* clean up allocations */
	sceGxmShaderPatcherReleaseFragmentProgram( s_shaderPatcher, s_clearFragmentProgram );
	sceGxmShaderPatcherReleaseVertexProgram( s_shaderPatcher, s_clearVertexProgram );
	graphicsFree( s_clearIndicesUId );
	graphicsFree( s_clearVerticesUId );

	/* wait until display queue is finished before deallocating display buffers */
	sceGxmDisplayQueueFinish();

	/* unregister programs and destroy shader patcher */
	sceGxmShaderPatcherUnregisterProgram( s_shaderPatcher, s_clearFragmentProgramId );
	sceGxmShaderPatcherUnregisterProgram( s_shaderPatcher, s_clearVertexProgramId );
	sceGxmShaderPatcherDestroy( s_shaderPatcher );
	fragmentUsseFree( s_patcherFragmentUsseUId );
	vertexUsseFree( s_patcherVertexUsseUId );
	graphicsFree( s_patcherBufferUId );
}

int GraphicsProcessor::shutdownGxm( void )
{
	/* ---------------------------------------------------------------------
	Finalize libgxm

	Once the GPU is finished, we deallocate all our memory,
	destroy all object and finally terminate libgxm.
	--------------------------------------------------------------------- */

	int returnCode = SCE_OK;

	graphicsFree( s_depthBufferUId );

	for ( unsigned int i = 0 ; i < DISPLAY_BUFFER_COUNT; ++i )
	{
		memset( s_displayBufferData[i], 0, DISPLAY_HEIGHT*DISPLAY_STRIDE_IN_PIXELS*4 );
		graphicsFree( s_displayBufferUId[i] );
		sceGxmSyncObjectDestroy( s_displayBufferSync[i] );
	}

	/* destroy the render target */
	sceGxmDestroyRenderTarget( s_renderTarget );

	/* destroy the context */
	sceGxmDestroyContext( s_context );

	fragmentUsseFree( s_fragmentUsseRingBufferUId );
	graphicsFree( s_fragmentRingBufferUId );
	graphicsFree( s_vertexRingBufferUId );
	graphicsFree( s_vdmRingBufferUId );
	free( s_contextParams.hostMem );

	/* terminate libgxm */
	sceGxmTerminate();

	return returnCode;
}

void* GraphicsProcessor::patcherHostAlloc( void *userData, uint32_t size )
{
	(void)( userData );
	return malloc( size );
}

void GraphicsProcessor::patcherHostFree( void *userData, void *mem )
{
	(void)( userData );
	free( mem );
}

void* GraphicsProcessor::graphicsAlloc( SceKernelMemBlockType type, uint32_t size, uint32_t alignment, uint32_t attribs, SceUID *uid )
{
	/*	Since we are using sceKernelAllocMemBlock directly, we cannot directly
	use the alignment parameter.  Instead, we must allocate the size to the
	minimum for this memblock type, and just SCE_DBG_ALWAYS_ASSERT that this will cover
	our desired alignment.

	Developers using their own heaps should be able to use the alignment
	parameter directly for more minimal padding.
	*/

	if( type == SCE_KERNEL_MEMBLOCK_TYPE_USER_CDRAM_RWDATA )
	{
		/* CDRAM memblocks must be 256KiB aligned */
		SCE_DBG_ALWAYS_ASSERT( alignment <= 256*1024 );
		size = ALIGN( size, 256*1024 );
	}
	else
	{
		/* LPDDR memblocks must be 4KiB aligned */
		SCE_DBG_ALWAYS_ASSERT( alignment <= 4*1024 );
		size = ALIGN( size, 4*1024 );
	}

	/* allocate some memory */
	*uid = sceKernelAllocMemBlock( "simple", type, size, NULL );
	SCE_DBG_ALWAYS_ASSERT( *uid >= SCE_OK );

	/* grab the base address */
	void *mem = NULL;
	int err = sceKernelGetMemBlockBase( *uid, &mem );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );

	/* map for the GPU */
	err = sceGxmMapMemory( mem, size, attribs );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );

	/* done */
	return mem;
}

void GraphicsProcessor::graphicsFree( SceUID uid )
{
	/* grab the base address */
	void *mem = NULL;
	int err = sceKernelGetMemBlockBase(uid, &mem);
	SCE_DBG_ALWAYS_ASSERT(err == SCE_OK);

	// unmap memory
	err = sceGxmUnmapMemory(mem);
	SCE_DBG_ALWAYS_ASSERT(err == SCE_OK);

	// free the memory block
	err = sceKernelFreeMemBlock(uid);
	SCE_DBG_ALWAYS_ASSERT(err == SCE_OK);
}

void* GraphicsProcessor::vertexUsseAlloc( uint32_t size, SceUID *uid, uint32_t *usseOffset )
{
	/* align to memblock alignment for LPDDR */
	size = ALIGN( size, 4096 );

	/* allocate some memory */
	*uid = sceKernelAllocMemBlock( "simple", SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, size, NULL );
	SCE_DBG_ALWAYS_ASSERT( *uid >= SCE_OK );

	/* grab the base address */
	void *mem = NULL;
	int err = sceKernelGetMemBlockBase( *uid, &mem );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );

	/* map as vertex USSE code for the GPU */
	err = sceGxmMapVertexUsseMemory( mem, size, usseOffset );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );

	return mem;
}

void GraphicsProcessor::vertexUsseFree( SceUID uid )
{
	/* grab the base address */
	void *mem = NULL;
	int err = sceKernelGetMemBlockBase( uid, &mem );
	SCE_DBG_ALWAYS_ASSERT(err == SCE_OK);

	/* unmap memory */
	err = sceGxmUnmapVertexUsseMemory( mem );
	SCE_DBG_ALWAYS_ASSERT(err == SCE_OK);

	/* free the memory block */
	err = sceKernelFreeMemBlock( uid );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );
}

void* GraphicsProcessor::fragmentUsseAlloc( uint32_t size, SceUID *uid, uint32_t *usseOffset )
{
	/* align to memblock alignment for LPDDR */
	size = ALIGN( size, 4096 );

	/* allocate some memory */
	*uid = sceKernelAllocMemBlock( "simple", SCE_KERNEL_MEMBLOCK_TYPE_USER_RWDATA_UNCACHE, size, NULL );
	SCE_DBG_ALWAYS_ASSERT( *uid >= SCE_OK );

	/* grab the base address */
	void *mem = NULL;
	int err = sceKernelGetMemBlockBase( *uid, &mem );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );

	/* map as fragment USSE code for the GPU */
	err = sceGxmMapFragmentUsseMemory( mem, size, usseOffset);
	SCE_DBG_ALWAYS_ASSERT(err == SCE_OK);

	// done
	return mem;
}

void GraphicsProcessor::fragmentUsseFree( SceUID uid )
{
	/* grab the base address */
	void *mem = NULL;
	int err = sceKernelGetMemBlockBase( uid, &mem );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );

	/* unmap memory */
	err = sceGxmUnmapFragmentUsseMemory( mem );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );

	/* free the memory block */
	err = sceKernelFreeMemBlock( uid );
	SCE_DBG_ALWAYS_ASSERT( err == SCE_OK );
}

void GraphicsProcessor::createCubicleFace(int faceIndex, int x, int y, float offset, float delta, uint32_t color)
{
	s_basicVertices[faceIndex][y][x][0].x = x + 0.0f;
	s_basicVertices[faceIndex][y][x][0].y = y + 0.0f;
	s_basicVertices[faceIndex][y][x][0].z = 0.0f + delta;

	s_basicVertices[faceIndex][y][x][1].x = x + 1.0f;
	s_basicVertices[faceIndex][y][x][1].y = y + 0.0f;
	s_basicVertices[faceIndex][y][x][1].z = 0.0f + delta;

	s_basicVertices[faceIndex][y][x][2].x = x + 1.0f;
	s_basicVertices[faceIndex][y][x][2].y = y + 1.0f;
	s_basicVertices[faceIndex][y][x][2].z = 0.0f + delta;

	s_basicVertices[faceIndex][y][x][3].x = x + 0.0f;
	s_basicVertices[faceIndex][y][x][3].y = y + 1.0f;
	s_basicVertices[faceIndex][y][x][3].z = 0.0f + delta;

	s_basicIndices[faceIndex][y][x][0] = 0;
	s_basicIndices[faceIndex][y][x][1] = 3;
	s_basicIndices[faceIndex][y][x][2] = 1;

	s_basicIndices[faceIndex][y][x][3] = 3;
	s_basicIndices[faceIndex][y][x][4] = 2;
	s_basicIndices[faceIndex][y][x][5] = 1;

	s_basicVertices[faceIndex][y][x][4].x = x + 0.0f + offset;
	s_basicVertices[faceIndex][y][x][4].y = y + 0.0f + offset;
	s_basicVertices[faceIndex][y][x][4].z = 0.0f;
	
	s_basicVertices[faceIndex][y][x][5].x = x + 1.0f - offset;
	s_basicVertices[faceIndex][y][x][5].y = y + 0.0f + offset;
	s_basicVertices[faceIndex][y][x][5].z = 0.0f;

	s_basicVertices[faceIndex][y][x][6].x = x + 1.0f - offset;
	s_basicVertices[faceIndex][y][x][6].y = y + 1.0f - offset;
	s_basicVertices[faceIndex][y][x][6].z = 0.0f;

	s_basicVertices[faceIndex][y][x][7].x = x + 0.0f + offset;
	s_basicVertices[faceIndex][y][x][7].y = y + 1.0f - offset;
	s_basicVertices[faceIndex][y][x][7].z = 0.0f;

	s_basicIndices[faceIndex][y][x][6] = 4;
	s_basicIndices[faceIndex][y][x][7] = 7;
	s_basicIndices[faceIndex][y][x][8] = 5;

	s_basicIndices[faceIndex][y][x][9] = 7;
	s_basicIndices[faceIndex][y][x][10] = 6;
	s_basicIndices[faceIndex][y][x][11] = 5;

	for (int i = 0; i < 8; i++)
	{
		s_basicVertices[faceIndex][y][x][i].nx = 0.0f;
		s_basicVertices[faceIndex][y][x][i].ny = 0.0f;
		s_basicVertices[faceIndex][y][x][i].nz = -1.0f;
		s_basicVertices[faceIndex][y][x][i].rotate = 0;
		s_basicVertices[faceIndex][y][x][i].color = 0xff000000;
	}

	for (int i = 4; i < 8; i++)
	{
		s_basicVertices[faceIndex][y][x][i].color = color;
	}
}

void GraphicsProcessor::createModelToWorld(int faceIndex)
{
	s_mtw[faceIndex] = Matrix4::identity();

	if (faceIndex == RubiksCube::back)
	{
		Matrix4 rotation = Matrix4::rotationX(PI);
		Matrix4 translation = Matrix4::translation(Vector3(0.0f, RubiksCube::SIZE, RubiksCube::SIZE));
		s_mtw[faceIndex] = translation * rotation;
	}
	else if (faceIndex == RubiksCube::left)
	{
		Matrix4 rotation = Matrix4::rotationY(PI / 2.0f);
		Matrix4 translation = Matrix4::translation(Vector3(-RubiksCube::SIZE, 0.0f, 0.0f));
		s_mtw[faceIndex] = rotation * translation;
	}
	else if (faceIndex == RubiksCube::right)
	{
		Matrix4 translation = Matrix4::translation(Vector3(RubiksCube::SIZE, 0.0f, 0.0f));
		Matrix4 rotation = Matrix4::rotationY(-PI / 2.0f);
		s_mtw[faceIndex] = translation * rotation;
	}
	else if (faceIndex == RubiksCube::up)
	{
		Matrix4 rotation = Matrix4::rotationX(-PI / 2.0f);
		Matrix4 translation = Matrix4::translation(Vector3(0.0f, 0.0f, RubiksCube::SIZE));
		s_mtw[faceIndex] =  translation * rotation;
	}
	else if (faceIndex == RubiksCube::down)
	{
		Matrix4 rotation = Matrix4::rotationX(PI / 2.0f);
		Matrix4 translation = Matrix4::translation(Vector3(0.0f, 0.0f, -RubiksCube::SIZE));
		s_mtw[faceIndex] =  rotation * translation;
	}
}

void GraphicsProcessor::processSwipe(int faceIndex, int x, int y, bool rotateX, Vector4 rotateAxis, float speed)
{
	if (faceIndex < 0 || faceIndex > 6) return;
	Vector3 axis = (s_mtw[faceIndex] * rotateAxis).getXYZ();
	s_rotate = Matrix4::rotation(speed, axis);

	if (rotateX && ((faceIndex == RubiksCube::front || faceIndex == RubiksCube::left || faceIndex == RubiksCube::right) && ((x >= 0 || x < 3) && y == 0) || ((faceIndex == RubiksCube::back) && ((x >= 0 || x < 3) && y == 2))))
	{
		for (int ix = 0; ix < 3; ix++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::front][0][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::left][0][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::right][0][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::back][2][ix][i].rotate = 1;
			}
		}

		for (int iy = 0; iy < 3; iy++)
			for (int ix = 0; ix < 3; ix++)
				for (int i = 0; i < 8; i++)
					s_basicVertices[RubiksCube::up][iy][ix][i].rotate = 1;
	}
	else if (rotateX && (faceIndex == RubiksCube::front || faceIndex == RubiksCube::left || faceIndex == RubiksCube::right || faceIndex == RubiksCube::back) && (x >= 0 || x < 3) && y == 1)
	{
		for (int ix = 0; ix < 3; ix++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::front][1][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::left][1][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::right][1][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::back][1][ix][i].rotate = 1;
			}
		}
	}
	else if (rotateX && ((faceIndex == RubiksCube::front || faceIndex == RubiksCube::left || faceIndex == RubiksCube::right) && ((x >= 0 || x < 3) && y == 2) || ((faceIndex == RubiksCube::back) && ((x >= 0 || x < 3) && y == 0))))
	{
		for (int ix = 0; ix < 3; ix++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::front][2][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::left][2][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::right][2][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::back][0][ix][i].rotate = 1;
			}
		}

		for (int iy = 0; iy < 3; iy++)
			for (int ix = 0; ix < 3; ix++)
				for (int i = 0; i < 8; i++)
					s_basicVertices[RubiksCube::down][iy][ix][i].rotate = 1;
	}
	else if (!rotateX && ((faceIndex == RubiksCube::front || faceIndex == RubiksCube::up || faceIndex == RubiksCube::down || faceIndex == RubiksCube::back) && (x == 0 && (y >= 0 || y < 3))))
	{
		for (int iy = 0; iy < 3; iy++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::front][iy][0][i].rotate = 1;
				s_basicVertices[RubiksCube::up][iy][0][i].rotate = 1;
				s_basicVertices[RubiksCube::down][iy][0][i].rotate = 1;
				s_basicVertices[RubiksCube::back][iy][0][i].rotate = 1;
			}
		}

		for (int iy = 0; iy < 3; iy++)
			for (int ix = 0; ix < 3; ix++)
				for (int i = 0; i < 8; i++)
					s_basicVertices[RubiksCube::left][iy][ix][i].rotate = 1;
	}
	else if (!rotateX && (faceIndex == RubiksCube::front || faceIndex == RubiksCube::up || faceIndex == RubiksCube::down || faceIndex == RubiksCube::back) && x == 1 && (y >= 0 || y < 3))
	{
		for (int iy = 0; iy < 3; iy++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::front][iy][1][i].rotate = 1;
				s_basicVertices[RubiksCube::up][iy][1][i].rotate = 1;
				s_basicVertices[RubiksCube::down][iy][1][i].rotate = 1;
				s_basicVertices[RubiksCube::back][iy][1][i].rotate = 1;
			}
		}
	}
	else if (!rotateX && ((faceIndex == RubiksCube::front || faceIndex == RubiksCube::up || faceIndex == RubiksCube::down || faceIndex == RubiksCube::back) && (x == 2 && (y >= 0 || y < 3))))
	{
		for (int iy = 0; iy < 3; iy++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::front][iy][2][i].rotate = 1;
				s_basicVertices[RubiksCube::up][iy][2][i].rotate = 1;
				s_basicVertices[RubiksCube::down][iy][2][i].rotate = 1;
				s_basicVertices[RubiksCube::back][iy][2][i].rotate = 1;
			}
		}

		for (int iy = 0; iy < 3; iy++)
			for (int ix = 0; ix < 3; ix++)
				for (int i = 0; i < 8; i++)
					s_basicVertices[RubiksCube::right][iy][ix][i].rotate = 1;
	}
	else if ((!rotateX && (faceIndex == RubiksCube::right && x == 0 && (y >= 0 || y < 3)) || (faceIndex == RubiksCube::left && x == 2 && (y >= 0 || y < 3)) || (rotateX && (faceIndex == RubiksCube::up && (x >= 0 || x < 3) && y == 2) || (faceIndex == RubiksCube::down && (x >= 0 || x < 3) && y == 0))))
	{
		for (int ix = 0; ix < 3; ix++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::right][ix][0][i].rotate = 1;
				s_basicVertices[RubiksCube::left][ix][2][i].rotate = 1;
				s_basicVertices[RubiksCube::up][2][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::down][0][ix][i].rotate = 1;
			}
		}

		for (int iy = 0; iy < 3; iy++)
			for (int ix = 0; ix < 3; ix++)
				for (int i = 0; i < 8; i++)
					s_basicVertices[RubiksCube::front][iy][ix][i].rotate = 1;
	}
	else if ((!rotateX && (faceIndex == RubiksCube::right || faceIndex == RubiksCube::left) && x == 1  && (y >= 0 || y <3)) || (rotateX && (faceIndex == RubiksCube::up || faceIndex == RubiksCube::down) && (x >= 0 || x < 3) && y == 1))
	{
		for (int ix = 0; ix < 3; ix++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::right][ix][1][i].rotate = 1;
				s_basicVertices[RubiksCube::left][ix][1][i].rotate = 1;
				s_basicVertices[RubiksCube::up][1][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::down][1][ix][i].rotate = 1;
			}
		}
	}
	else if ((!rotateX && (faceIndex == RubiksCube::right && x == 2 && (y >= 0 || y < 3)) || (faceIndex == RubiksCube::left && x == 0 && (y >= 0 || y < 3)) || (rotateX && (faceIndex == RubiksCube::up && (x >= 0 || x < 3) && y == 0) || (faceIndex == RubiksCube::down && (x >= 0 || x < 3) && y == 2))))
	{
		for (int ix = 0; ix < 3; ix++)
		{
			for (int i = 0; i < 8; i++)
			{
				s_basicVertices[RubiksCube::right][ix][2][i].rotate = 1;
				s_basicVertices[RubiksCube::left][ix][0][i].rotate = 1;
				s_basicVertices[RubiksCube::up][0][ix][i].rotate = 1;
				s_basicVertices[RubiksCube::down][2][ix][i].rotate = 1;
			}
		}

		for (int iy = 0; iy < 3; iy++)
			for (int ix = 0; ix < 3; ix++)
				for (int i = 0; i < 8; i++)
					s_basicVertices[RubiksCube::back][iy][ix][i].rotate = 1;
	}
}
