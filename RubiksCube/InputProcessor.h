#pragma once
#include <ctrl.h>
#include <touch.h>
#include <systemgesture.h>
#include <libsysmodule.h>

#include "definitions.h"
#include "RubiksCube.h"
#include "GraphicsProcessor.h"

using namespace sce::Vectormath::Simd::Aos;

class InputProcessor
{
public:
	enum State { NO_MOVEMENT = 0, SWIPING = 1, SNAPPING = 2 };

	static float turningAngleX;
	static float turningAngleY;
	static float turningAngleZ;
	static float accumulatedScale;
	static State state;
	static int hitFaceIndex;
	static int hitIndexX;
	static int hitIndexY;
	static Vector4 rotateAxis;
	static bool rotateX;
	static float swipeSpeed;
	static bool swipe;

	InputProcessor();
	~InputProcessor(void);

	void processInputs();
private:
	static const float DEADZONE = 0.1f;
	static const float ROTATION_DAMPENING = 0.01f;
	static const float ROTATION_SPEED = 5.0f;
	static const float PINCH_SPEED = 0.01f * 0.25f;
	static const float PINCH_MIN = 0.8f;
	static const float PINCH_MAX = 1.2f;
	static const float TOUCH_DISTANCE_THRESHOLD = 0.1f;

	int result;
	bool inputProcessed;
	SceCtrlData ct;
	SceSystemGestureTouchRecognizer pinchOutInRecognizer;
	SceSystemGestureTouchRecognizer dragRecognizer;
	SceTouchData tdf, tdb;
	SceSystemGestureTouchEvent te;
	int eventCount;
	
	int lastTouchId;
	Vector4 hitU;
	Vector4 hitV;
	Vector2 firstTouch;
	Vector2 lastTouch;
	Vector2 deltaTouch;
	Vector2 z;
	bool rotationAxisDetermined;

	float makeFloat(unsigned char input);
	float makeFloatX(float input);
	float makeFloatY(float input);

	void processAnalogStickInputs();
	void processButtonInputs();
	void processPinchInputs();
	void processDragInputs();
	void processSwipeInputs();
	void excecuteSwipe(int faceIndex, int x, int y, bool rotateX, float speed);

	void performHitTest();
	void determineRotationAxis();
	void determineRotationSpeed();
};

