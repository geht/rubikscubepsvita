#pragma once
#include <assert.h>
#include <stdio.h>

class RubiksCube
{
public:
	static const int SIZE = 3;

	// enum Color { WHITE = 0xffffff, YELLOW = 0xffd500, ORANGE = 0xff5800, RED = 0xb71234 , GREEN = 0x009b48, BLUE = 0x0046ad };
	enum Color { WHITE = 0xffffffff, YELLOW = 0xff00d5ff, ORANGE = 0xff0058ff, RED = 0xff3412b7, GREEN = 0xff489b00, BLUE = 0xffad4600 };

	static int front;
	static int back;
	static int up;
	static int down;
	static int left;
	static int right;

	static Color cube[6][SIZE][SIZE];

	RubiksCube(void);
	~RubiksCube(void);
	
	// Moves in Singmaster notation
	static void F();
	static void FP();
	static void B();
	static void BP();
	static void U();
	static void UP();
	static void D();
	static void DP();
	static void L();
	static void LP();
	static void R();
	static void RP();
	static void M();
	static void MP();
	static void E();
	static void EP();
	static void S();
	static void SP();

	static void f();
	static void fP();
	static void b();
	static void bP();
	static void u();
	static void uP();
	static void d();
	static void dP();
	static void l();
	static void lP();
	static void r();
	static void rP();
	static void X();
	static void XP();
	static void Y();
	static void YP();
	static void Z();
	static void ZP();

	static void print();
private:
	static void init();
	static void initFace(int faceIndex, Color color);
	static void turnFace(int faceIndex);
	static void turnFaceP(int faceIndex);
	static void printFace(int faceIndex);
	static char* getColorName(Color color);
};

