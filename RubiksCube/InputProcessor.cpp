#include "InputProcessor.h"

float InputProcessor::turningAngleX = 0.0f;
float InputProcessor::turningAngleY = 0.0f;
float InputProcessor::turningAngleZ = 0.0f;
float InputProcessor::accumulatedScale = 1.0f;
InputProcessor::State InputProcessor::state = NO_MOVEMENT;
int InputProcessor::hitFaceIndex = -1;
int InputProcessor::hitIndexX;
int InputProcessor::hitIndexY;
Vector4 InputProcessor::rotateAxis;
bool InputProcessor::rotateX;
float InputProcessor::swipeSpeed;
bool InputProcessor::swipe;

InputProcessor::InputProcessor()
{
	sceCtrlSetSamplingMode(SCE_CTRL_MODE_DIGITALANALOG);
	sceTouchSetSamplingState(SCE_TOUCH_PORT_FRONT, SCE_TOUCH_SAMPLING_STATE_START);
	sceTouchSetSamplingState(SCE_TOUCH_PORT_BACK, SCE_TOUCH_SAMPLING_STATE_START);
	sceSysmoduleLoadModule(SCE_SYSMODULE_SYSTEM_GESTURE);
	sceSystemGestureInitializePrimitiveTouchRecognizer(NULL);
	sceSystemGestureCreateTouchRecognizer( &pinchOutInRecognizer, SCE_SYSTEM_GESTURE_TYPE_PINCH_OUT_IN, SCE_TOUCH_PORT_FRONT, NULL, NULL );
	sceSystemGestureCreateTouchRecognizer( &dragRecognizer, SCE_SYSTEM_GESTURE_TYPE_DRAG, SCE_TOUCH_PORT_BACK, NULL, NULL );
}


InputProcessor::~InputProcessor(void)
{
}

void InputProcessor::processInputs()
{
	result = sceCtrlReadBufferPositive(0, &ct, 1);
	sceTouchRead(SCE_TOUCH_PORT_FRONT, &tdf, 1);
	sceTouchRead(SCE_TOUCH_PORT_BACK, &tdb, 1);
	sceSystemGestureUpdatePrimitiveTouchRecognizer(&tdf, &tdb);
	sceSystemGestureUpdateTouchRecognizer(&pinchOutInRecognizer);
	sceSystemGestureUpdateTouchRecognizer(&dragRecognizer);

	processAnalogStickInputs();
	// processButtonInputs();
	processPinchInputs();
	processDragInputs();
	processSwipeInputs();
}

void InputProcessor::processAnalogStickInputs()
{
	if (result < 0) return;
	if (state != NO_MOVEMENT)
	{
		turningAngleX = 0.0f;
		turningAngleY = 0.0f;
		return;
	}

	float x = makeFloat(ct.lx);
	float y = makeFloat(ct.ly);
	float z = makeFloat(ct.rx);

	if (x >= DEADZONE || x <= -DEADZONE)
		turningAngleX = x * ROTATION_DAMPENING * ROTATION_SPEED;
	else
		turningAngleX = 0.0f;
	if (y >= DEADZONE || y <= -DEADZONE)
		turningAngleY = y * ROTATION_DAMPENING * ROTATION_SPEED;
	else
		turningAngleY = 0.0f;
	if (z >= DEADZONE || z <= -DEADZONE)
		turningAngleZ = z * ROTATION_DAMPENING * ROTATION_SPEED;
	else
		turningAngleZ = 0.0f;
}

void InputProcessor::processButtonInputs()
{
	if (result < 0 || state != NO_MOVEMENT) return;
	if (!inputProcessed) {
		if ((ct.buttons & SCE_CTRL_LEFT) != 0) {
			RubiksCube::EP();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_RIGHT) != 0) {
			RubiksCube::E();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_UP) != 0) {
			RubiksCube::MP();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_DOWN) != 0) {
			RubiksCube::M();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_TRIANGLE) != 0) {
			RubiksCube::U();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_CROSS) != 0) {
			RubiksCube::D();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_SQUARE) != 0) {
			RubiksCube::L();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_CIRCLE) != 0) {
			RubiksCube::R();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_L) != 0) {
			RubiksCube::F();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_R) != 0) {
			RubiksCube::B();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_START) != 0) {
			RubiksCube::R();
			RubiksCube::U();
			RubiksCube::RP();
			RubiksCube::UP();
			inputProcessed = true;
		}

		if ((ct.buttons & SCE_CTRL_SELECT)) {
			RubiksCube::print();
			inputProcessed = true;
		}
	} else if (!ct.buttons)
	{
		inputProcessed = false;
	}
}

void InputProcessor::processPinchInputs()
{
	if (state != NO_MOVEMENT) return;

	eventCount = sceSystemGestureGetTouchEventsCount(&pinchOutInRecognizer);
	for(int i = 0; i < eventCount; i++)
	{
		sceSystemGestureGetTouchEventByIndex(&pinchOutInRecognizer, 0, &te);
		float scale = te.property.pinchOutIn.scale;
		int deltaX = te.property.pinchOutIn.primitive[0].deltaVector.x;
		int deltaY = te.property.pinchOutIn.primitive[0].deltaVector.y;

		if (deltaX != 0 || deltaY != 0)
		{
			if (scale < 1.0f)
				accumulatedScale += abs(deltaX - deltaY) * PINCH_SPEED;
			else
				accumulatedScale -= abs(deltaX + deltaY) * PINCH_SPEED;

			if (accumulatedScale <= PINCH_MIN)
				accumulatedScale = PINCH_MIN;
			if (accumulatedScale >= PINCH_MAX)
				accumulatedScale = PINCH_MAX;
		}
	}
}

void InputProcessor::processDragInputs()
{
	if (state != NO_MOVEMENT) return;

	eventCount = sceSystemGestureGetTouchEventsCount(&dragRecognizer);
	for(int i = 0; i < eventCount; i++)
	{
	  sceSystemGestureGetTouchEventByIndex(&dragRecognizer, i, &te);
	  turningAngleX = te.property.drag.deltaVector.x / (float)DISPLAY_WIDTH * ROTATION_SPEED;
	  turningAngleY = te.property.drag.deltaVector.y / (float)DISPLAY_HEIGHT * ROTATION_SPEED;
	}
}

void InputProcessor::processSwipeInputs()
{
	if (tdf.reportNum == 1 && state != SNAPPING)
	{
		int id = tdf.report[0].id;

		if (id != lastTouchId)
		{
			lastTouchId = id;
			hitFaceIndex = -1;
			rotationAxisDetermined = false;
			firstTouch = Vector2(makeFloatX(tdf.report[0].x), makeFloatY(tdf.report[0].y));
		}
		else
		{
			Vector2 newTouch = Vector2(makeFloatX(tdf.report[0].x), makeFloatY(tdf.report[0].y));
			deltaTouch = newTouch - lastTouch;
			lastTouch = newTouch;

			performHitTest();

			if (hitFaceIndex != -1) // if hit
			{
				z = lastTouch - firstTouch;
				float len = length(z);

				if (len > TOUCH_DISTANCE_THRESHOLD)
				{
					state = SWIPING;

					if (!rotationAxisDetermined)
						determineRotationAxis();

					determineRotationSpeed();
				}
			}
		}
	}
	else if (tdf.reportNum == 0 && state == SWIPING)
	{
		state = SNAPPING;
	}
	else if (state == SNAPPING && !GraphicsProcessor::animationInProgress)
	{
		if (swipe)
			excecuteSwipe(hitFaceIndex, hitIndexX, hitIndexY, rotateX, -swipeSpeed);

		state = NO_MOVEMENT;
		swipe = false;
	}
}

void InputProcessor::performHitTest()
{
	Matrix4 invWvp = inverse(GraphicsProcessor::s_wvp);
	Vector4 p = Vector4((tdf.report[0].x / (float)DISPLAY_WIDTH) - 1.0f, 1.0f - (tdf.report[0].y / (float)DISPLAY_HEIGHT), 0.9f, 1.0f);
	Vector4 q = Vector4((tdf.report[0].x / (float)DISPLAY_WIDTH) - 1.0f, 1.0f - (tdf.report[0].y / (float)DISPLAY_HEIGHT), 0.1f, 1.0f);

	p = invWvp * p;
	p /= p.getW();
	q = invWvp * q;
	q /= q.getW();

	Vector4 d = q - p;

	for (int i = 0; i < 6; i++)
	{
		Matrix4 mtw = GraphicsProcessor::s_mtw[i];
		Vector4 n = Vector4(GraphicsProcessor::s_basicVertices[i][0][0][0].nx, GraphicsProcessor::s_basicVertices[i][0][0][0].ny, GraphicsProcessor::s_basicVertices[i][0][0][0].nz, 0.0f);
		n = mtw * n;
		n = normalize(n);

		if (dot(d, n) > 0)
		{
			Vector4 o = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
			Vector4 u = Vector4(1.0f, 0.0f, 0.0f, 0.0f);
			Vector4 v = Vector4(0.0f, 1.0f, 0.0f, 0.0f);

			o = mtw * o;
			o /= o.getW();

			u = mtw * u;
			u = normalize(u);

			v = mtw * v;
			v = normalize(v);
			
			float alpha = - (dot(p - o, n) / dot(d, n));
			Vector4 s = p + alpha * d;

			float x = dot(u, s - o);
			float y = dot(v, s - o);

			if (x > 0 - EPSILON && x < 3 - EPSILON && y > 0 - EPSILON && y < 3 - EPSILON)
			{
				Vector4 newHit = Vector4(x, y, 0.0f, 1.0f);
				if (hitFaceIndex == -1)
				{
					hitFaceIndex = i;
					hitIndexX = (int)floor(x);
					hitIndexY = (int)floor(y);
					hitU = u;
					hitV = v;
				}
			}
		}
	}
}

void InputProcessor::determineRotationAxis()
{
	Vector4 h4 = GraphicsProcessor::s_wvp * hitU;
	Vector4 k4 = GraphicsProcessor::s_wvp * hitV;

	Vector2 h = h4.getXY();
	Vector2 k = k4.getXY();

	h = normalize(h);
	k = normalize(k);
	z = normalize(z);

	if (abs(dot(h, z)) > abs(dot(k, z)))
	{
		rotationAxisDetermined = true;
		rotateAxis = Vector4(0.0f, 1.0f, 0.0f, 0.0f);
		rotateX = true;
	}
	else if (abs(dot(h, z)) < abs(dot(k, z)))
	{
		rotationAxisDetermined = true;
		rotateAxis = Vector4(1.0f, 0.0f, 0.0f, 0.0f);
		rotateX = false;
	}
}

void InputProcessor::determineRotationSpeed()
{
	if (rotateX)
	{
		Vector4 u4 = GraphicsProcessor::s_wvp * hitU;
		Vector2 u = u4.getXY();
		Vector2 speedVec = firstTouch - lastTouch;
		speedVec.setY(speedVec.getY() / 2.0f);
		swipeSpeed = dot(u, speedVec);
	}
	else
	{
		Vector4 v4 = GraphicsProcessor::s_wvp * hitV;
		Vector2 v = v4.getXY();
		Vector2 speedVec = lastTouch - firstTouch;
		speedVec.setY(speedVec.getY() / 2.0f);
		swipeSpeed = dot(v, speedVec);
	}
}

void InputProcessor::excecuteSwipe(int faceIndex, int x, int y, bool rotateX, float speed)
{
	if (faceIndex < 0 || faceIndex > 6) return;
	if (faceIndex == RubiksCube::front)
	{
		if ((x >= 0 || x < 3) && y == 0 && rotateX && speed < 0.0f)
			RubiksCube::U();
		else if ((x >= 0 || x < 3) && y == 0 && rotateX && speed > 0.0f)
			RubiksCube::UP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed < 0.0f)
			RubiksCube::EP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed > 0.0f)
			RubiksCube::E();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed < 0.0f)
			RubiksCube::DP();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed > 0.0f)
			RubiksCube::D();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::L();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::LP();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::M();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::MP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::RP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::R();
	}
	else if (faceIndex == RubiksCube::right)
	{
		if ((x >= 0 || x < 3) && y == 0 && rotateX && speed < 0.0f)
			RubiksCube::U();
		else if ((x >= 0 || x < 3) && y == 0 && rotateX && speed > 0.0f)
			RubiksCube::UP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed < 0.0f)
			RubiksCube::EP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed > 0.0f)
			RubiksCube::E();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed < 0.0f)
			RubiksCube::DP();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed > 0.0f)
			RubiksCube::D();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::F();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::FP();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::S();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::SP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::BP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::B();
	}
	else if (faceIndex == RubiksCube::left)
	{
		if ((x >= 0 || x < 3) && y == 0 && rotateX && speed < 0.0f)
			RubiksCube::U();
		else if ((x >= 0 || x < 3) && y == 0 && rotateX && speed > 0.0f)
			RubiksCube::UP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed < 0.0f)
			RubiksCube::EP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed > 0.0f)
			RubiksCube::E();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed < 0.0f)
			RubiksCube::DP();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed > 0.0f)
			RubiksCube::D();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::FP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::F();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::SP();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::S();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::B();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::BP();
	}
	else if (faceIndex == RubiksCube::up)
	{
		if ((x >= 0 || x < 3) && y == 0 && rotateX && speed < 0.0f)
			RubiksCube::B();
		else if ((x >= 0 || x < 3) && y == 0 && rotateX && speed > 0.0f)
			RubiksCube::BP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed < 0.0f)
			RubiksCube::SP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed > 0.0f)
			RubiksCube::S();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed < 0.0f)
			RubiksCube::FP();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed > 0.0f)
			RubiksCube::F();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::L();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::LP();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::M();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::MP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::RP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::R();
		return;
	}
	else if (faceIndex == RubiksCube::down)
	{
		if ((x >= 0 || x < 3) && y == 0 && rotateX && speed < 0.0f)
			RubiksCube::F();
		else if ((x >= 0 || x < 3) && y == 0 && rotateX && speed > 0.0f)
			RubiksCube::FP();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed < 0.0f)
			RubiksCube::S();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed > 0.0f)
			RubiksCube::SP();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed < 0.0f)
			RubiksCube::BP();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed > 0.0f)
			RubiksCube::B();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::L();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::LP();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::M();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::MP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::RP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::R();
	}
	else if (faceIndex == RubiksCube::back)
	{
		if ((x >= 0 || x < 3) && y == 2 && rotateX && speed < 0.0f)
			RubiksCube::UP();
		else if ((x >= 0 || x < 3) && y == 2 && rotateX && speed > 0.0f)
			RubiksCube::U();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed < 0.0f)
			RubiksCube::E();
		else if ((x >= 0 || x < 3) && y == 1 && rotateX && speed > 0.0f)
			RubiksCube::EP();
		else if ((x >= 0 || x < 3) && y == 0 && rotateX && speed < 0.0f)
			RubiksCube::D();
		else if ((x >= 0 || x < 3) && y == 0 && rotateX && speed > 0.0f)
			RubiksCube::DP();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::L();
		else if (x == 0 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::LP();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::M();
		else if (x == 1 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::MP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed < 0.0f)
			RubiksCube::RP();
		else if (x == 2 && (y >= 0 || y < 3) && !rotateX && speed > 0.0f)
			RubiksCube::R();
	}
}

float InputProcessor::makeFloat(unsigned char input)
{
    return (((float)(input)) / 255.0f * 2.0f) - 1.0f;
}

float InputProcessor::makeFloatX(float input)
{
	return (input / (float)DISPLAY_WIDTH) - 1.0f;
}

float InputProcessor::makeFloatY(float input)
{
	return 1.0f - (input / (float)DISPLAY_HEIGHT);
}
