#include "SoundProcessor.h"

float SoundProcessor::lastSpeed = 0.0f;

SoundProcessor::SoundProcessor(void)
{
	int returnCode = SCE_OK;

	// init part
	returnCode = init();
	SCE_DBG_ASSERT(SCE_OK == returnCode);
	printf("## pcm_player: INIT SUCCEEDED ##\n");
}

SoundProcessor::~SoundProcessor(void)
{
	int returnCode = SCE_OK;

	// shutdown part
	returnCode = shutdown();
	SCE_DBG_ASSERT(SCE_OK == returnCode);
	printf("## pcm_player: FINISHED ##\n");
}

void SoundProcessor::processSound()
{
	// int returnCode = SCE_OK;

	// main part
	// returnCode = process();
	// SCE_DBG_ASSERT(SCE_OK == returnCode);
	
	float newSpeed;
	float deltaSpeed;

	switch (InputProcessor::state)
	{
		case InputProcessor::NO_MOVEMENT:
			break;
		case InputProcessor::SWIPING:
			newSpeed = abs(InputProcessor::swipeSpeed);
			deltaSpeed = abs(newSpeed - lastSpeed);
			lastSpeed = newSpeed;

			if (deltaSpeed >= 0.001425f * DAMPENING)
				sceNgsVoicePlay( voicePlayerSFX2 );
			else
				sceNgsVoiceKill( voicePlayerSFX2 );
			break;
		case InputProcessor::SNAPPING:
			sceNgsVoiceKill( voicePlayerSFX2 );
			sceNgsVoiceKill( voicePlayerSFX2 );
			sceNgsVoiceKill( voicePlayerSFX );
			sceNgsVoicePlay( voicePlayerSFX );
			break;
	}
}

int SoundProcessor::init(void)
{
	int          returnCode  = SCE_OK;
	SceNgsHVoice voicePlayerMusic = NULL;
	voicePlayerSFX = NULL;
	voicePlayerSFX2 = NULL;

	// Load PCM data
	returnCode = loadWAVFile( INPUT_FILE_NAME, &s_music );
	if ( returnCode != SCE_OK ) {
		printf( " Failed to load WAV file \"%s\".\n", INPUT_FILE_NAME );
		return returnCode;
	}
	printf( " WAV file \"%s\" has been loaded\n", INPUT_FILE_NAME );

	returnCode = loadWAVFile( INPUT_FILE_NAME2, &s_sfx );
	if ( returnCode != SCE_OK ) {
		printf( " Failed to load WAV file \"%s\".\n", INPUT_FILE_NAME2 );
		return returnCode;
	}
	printf( " WAV file \"%s\" has been loaded\n", INPUT_FILE_NAME2 );

	returnCode = loadWAVFile( INPUT_FILE_NAME3, &s_sfx2 );
	if ( returnCode != SCE_OK ) {
		printf( " Failed to load WAV file \"%s\".\n", INPUT_FILE_NAME3 );
		return returnCode;
	}
	printf( " WAV file \"%s\" has been loaded\n", INPUT_FILE_NAME3 );

	// Initialise NGS
	returnCode = initNGS();
	if ( returnCode != SCE_OK ) {
		return returnCode;
	}
	printf( "....init'd NGS\n" );


	// Create racks
	returnCode = createRacks( s_music.nNumChannels, s_sfx.nNumChannels, s_sfx2.nNumChannels );
	if ( returnCode != SCE_OK ) {
		return returnCode;
	}
	printf( "....created Racks\n" );


	// Get voice handles
	returnCode = sceNgsRackGetVoiceHandle( s_rackPlayerMusic, 0, &voicePlayerMusic );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "init: sceNgsRackGetVoiceHandle(player music) failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	returnCode = sceNgsRackGetVoiceHandle( s_rackPlayerSFX, 0, &voicePlayerSFX );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "init: sceNgsRackGetVoiceHandle(player sfx) failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	returnCode = sceNgsRackGetVoiceHandle( s_rackPlayerSFX2, 0, &voicePlayerSFX2 );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "init: sceNgsRackGetVoiceHandle(player sfx 2) failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	returnCode = sceNgsRackGetVoiceHandle( s_rackMaster, 0, &s_voiceMaster );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "init: sceNgsRackGetVoiceHandle(master) failed: 0x%08X\n", returnCode );
		return returnCode;
	}


	// Initialise player
	returnCode = initPlayer( voicePlayerMusic, SCE_NGS_VOICE_T1_PCM_PLAYER, &s_music, SCE_NGS_PLAYER_LOOP_CONTINUOUS );
	if ( returnCode != SCE_OK ) {
		return returnCode;
	}
	returnCode = initPlayer( voicePlayerSFX, SCE_NGS_VOICE_T1_PCM_PLAYER, &s_sfx, 0 );
	if ( returnCode != SCE_OK ) {
		return returnCode;
	}
	returnCode = initPlayer( voicePlayerSFX2, SCE_NGS_VOICE_T1_PCM_PLAYER, &s_sfx2, 0 );
	if ( returnCode != SCE_OK ) {
		return returnCode;
	}
	printf( "....init'd Player\n" );


	// Connect racks
	returnCode = connectRacks( voicePlayerMusic, s_voiceMaster );
	returnCode = connectRacks( voicePlayerSFX, s_voiceMaster );
	returnCode = connectRacks( voicePlayerSFX2, s_voiceMaster );
	if ( returnCode != SCE_OK ) {
		return returnCode;
	}
	printf( "....connected Racks\n" );


	// Play voices
	returnCode = sceNgsVoicePlay( voicePlayerMusic );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "init: sceNgsVoicePlay(player music) failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	returnCode = sceNgsVoicePlay( s_voiceMaster );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "init: sceNgsVoicePlay(master) failed: 0x%08X\n", returnCode );
		return returnCode;
	}


	// Prepare device and/or file for audio output
	returnCode = prepareAudioOut( OUTPUT_MODE, SYS_GRANULARITY, SYS_SAMPLE_RATE, OUTPUT_FILE_NAME );
	if ( returnCode != SCE_NGS_OK ) {
		return returnCode;
	}


	// launch high priority thread for NGS update and audio output
	returnCode = startAudioUpdateThread();
	if ( returnCode != SCE_NGS_OK ) {
		return returnCode;
	}


	return returnCode;
}

int SoundProcessor::initNGS(void)
{
	int                    returnCode = SCE_OK;
	SceNgsSystemInitParams initParams;
	size_t                 size;

	// initialize NGS lib
	returnCode = sceSysmoduleLoadModule( SCE_SYSMODULE_NGS );
	if ( returnCode != SCE_OK ) {
		printf( "initNGS: sceSysmoduleLoadModule(NGS) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	initParams.nMaxRacks    = 4;
	initParams.nMaxVoices   = 4;
	initParams.nGranularity = SYS_GRANULARITY;
	initParams.nSampleRate  = SYS_SAMPLE_RATE;
	initParams.nMaxModules  = NUM_MODULES;

	// Determine memory requirement
	returnCode = sceNgsSystemGetRequiredMemorySize( &initParams, &size );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "initNGS: sceNgsSystemGetRequiredMemorySize() failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Allocate memory
	s_pSysMem = memalign( SCE_NGS_MEMORY_ALIGN_SIZE, size );
	if ( s_pSysMem == NULL ) {
		printf( "initNGS: malloc() failed for %d bytes\n", size );
		return SCE_ERROR_ERRNO_ENOMEM;
	}

	// Initialise NGS and get system handle
	returnCode = sceNgsSystemInit( s_pSysMem, size, &initParams, &s_sysHandle );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "initNGS: sceNgsSystemInit() failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	printf( "...sceNgsSystemInit() OK\n" );

	return returnCode;
}

int SoundProcessor::createRacks( int nChannels, int nChannels2, int nChannels3 )
{
	int returnCode;

	const struct SceNgsVoiceDefinition *pT1VoiceDef;
	const struct SceNgsVoiceDefinition *pMasterBussVoiceDef;
	SceNgsRackDescription rackDesc;
	SceNgsBufferInfo      bufferInfo;

	// Get voice definitions
	pT1VoiceDef         = sceNgsVoiceDefGetTemplate1();
	pMasterBussVoiceDef = sceNgsVoiceDefGetMasterBuss();

	// Determine memory requirement for music player rack
	rackDesc.nChannelsPerVoice   = nChannels;
	rackDesc.nVoices             = 1;
	rackDesc.pVoiceDefn          = pT1VoiceDef;
	rackDesc.nMaxPatchesPerInput = 0;
	rackDesc.nPatchesPerOutput   = 1;

	returnCode = sceNgsRackGetRequiredMemorySize( s_sysHandle, &rackDesc, &bufferInfo.size);
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackGetRequiredMemorySize(music player) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Allocate memory for music player rack
	s_pRackMemPlayerMusic = memalign( SCE_NGS_MEMORY_ALIGN_SIZE, bufferInfo.size );
	if ( s_pRackMemPlayerMusic == NULL ) {
		printf( "createRacks: malloc(player_rack_music) failed for %d bytes\n", bufferInfo.size );
		return SCE_ERROR_ERRNO_ENOMEM;
	}
	memset( s_pRackMemPlayerMusic, 0, bufferInfo.size );

	// Initialise music player rack
	bufferInfo.data = s_pRackMemPlayerMusic;
	returnCode = sceNgsRackInit( s_sysHandle, &bufferInfo, &rackDesc, &s_rackPlayerMusic );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackInit(music player) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Determine memory requirement for sfx player rack
	rackDesc.nChannelsPerVoice   = nChannels2;
	rackDesc.nVoices             = 1;
	rackDesc.pVoiceDefn          = pT1VoiceDef;
	rackDesc.nMaxPatchesPerInput = 0;
	rackDesc.nPatchesPerOutput   = 1;

	returnCode = sceNgsRackGetRequiredMemorySize( s_sysHandle, &rackDesc, &bufferInfo.size);
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackGetRequiredMemorySize(sfx player) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Allocate memory for sfx player rack
	s_pRackMemPlayerSFX = memalign( SCE_NGS_MEMORY_ALIGN_SIZE, bufferInfo.size );
	if ( s_pRackMemPlayerSFX == NULL ) {
		printf( "createRacks: malloc(player_rack_sfx) failed for %d bytes\n", bufferInfo.size );
		return SCE_ERROR_ERRNO_ENOMEM;
	}
	memset( s_pRackMemPlayerSFX, 0, bufferInfo.size );

	// Initialise sfx player rack
	bufferInfo.data = s_pRackMemPlayerSFX;
	returnCode = sceNgsRackInit( s_sysHandle, &bufferInfo, &rackDesc, &s_rackPlayerSFX );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackInit(sfx player) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Determine memory requirement for sfx player rack 2
	rackDesc.nChannelsPerVoice   = nChannels3;
	rackDesc.nVoices             = 1;
	rackDesc.pVoiceDefn          = pT1VoiceDef;
	rackDesc.nMaxPatchesPerInput = 0;
	rackDesc.nPatchesPerOutput   = 1;

	returnCode = sceNgsRackGetRequiredMemorySize( s_sysHandle, &rackDesc, &bufferInfo.size);
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackGetRequiredMemorySize(sfx player 2) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Allocate memory for sfx player rack 2
	s_pRackMemPlayerSFX2 = memalign( SCE_NGS_MEMORY_ALIGN_SIZE, bufferInfo.size );
	if ( s_pRackMemPlayerSFX2 == NULL ) {
		printf( "createRacks: malloc(player_rack_sfx 2) failed for %d bytes\n", bufferInfo.size );
		return SCE_ERROR_ERRNO_ENOMEM;
	}
	memset( s_pRackMemPlayerSFX2, 0, bufferInfo.size );

	// Initialise sfx player rack 2
	bufferInfo.data = s_pRackMemPlayerSFX2;
	returnCode = sceNgsRackInit( s_sysHandle, &bufferInfo, &rackDesc, &s_rackPlayerSFX2 );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackInit(sfx player 2) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Determine memory requirement for master rack
	rackDesc.nChannelsPerVoice      = 2;
	rackDesc.nVoices                = 1;
	rackDesc.pVoiceDefn             = pMasterBussVoiceDef;
	rackDesc.nMaxPatchesPerInput    = 3;
	rackDesc.nPatchesPerOutput      = 0;

	returnCode = sceNgsRackGetRequiredMemorySize( s_sysHandle, &rackDesc, &bufferInfo.size);
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackGetRequiredMemorySize(master) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Allocate memory for master rack
	s_pRackMemMaster = memalign( SCE_NGS_MEMORY_ALIGN_SIZE, bufferInfo.size );
	if ( s_pRackMemMaster == NULL ) {
		printf( "createRacks: malloc(master_rack) failed for %d bytes\n", bufferInfo.size );
		return SCE_ERROR_ERRNO_ENOMEM;
	}
	memset( s_pRackMemMaster, 0, bufferInfo.size );

	// Initialise master rack
	bufferInfo.data = s_pRackMemMaster;
	returnCode = sceNgsRackInit( s_sysHandle, &bufferInfo, &rackDesc, &s_rackMaster );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "createRacks: sceNgsRackInit(master) failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	return SCE_OK;
}

int SoundProcessor::initPlayer( SceNgsHVoice hVoice, SceUInt32 nModuleId, SoundInfo *pSound, int loopCount )
{
	int                returnCode;
	SceNgsBufferInfo   bufferInfo;
	SceNgsPlayerParams *pPcmParams;

	// Get player parameters
	returnCode = sceNgsVoiceLockParams( hVoice, nModuleId, SCE_NGS_PLAYER_PARAMS_STRUCT_ID, &bufferInfo );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "initPlayer: sceNgsVoiceLockParams() failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Set player parameters
	memset( bufferInfo.data, 0, bufferInfo.size);
	pPcmParams = (SceNgsPlayerParams *)bufferInfo.data;
	pPcmParams->desc.id     = SCE_NGS_PLAYER_PARAMS_STRUCT_ID;
	pPcmParams->desc.size   = sizeof(SceNgsPlayerParams);

	pPcmParams->fPlaybackFrequency          = (SceFloat32)pSound->nSampleRate;
	pPcmParams->fPlaybackScalar             = 1.0f;
	pPcmParams->nLeadInSamples              = 0;
	pPcmParams->nLimitNumberOfSamplesPlayed = 0;
	pPcmParams->nChannels                   = pSound->nNumChannels;
	if ( pSound->nNumChannels == 1 ) {
		pPcmParams->nChannelMap[0] = SCE_NGS_PLAYER_LEFT_CHANNEL;
		pPcmParams->nChannelMap[1] = SCE_NGS_PLAYER_LEFT_CHANNEL;
	} else {
		pPcmParams->nChannelMap[0] = SCE_NGS_PLAYER_LEFT_CHANNEL;
		pPcmParams->nChannelMap[1] = SCE_NGS_PLAYER_RIGHT_CHANNEL;
	}
	pPcmParams->nType            = pSound->nType;
	pPcmParams->nStartBuffer     = 0;
	pPcmParams->nStartByte       = 0;

	pPcmParams->buffs[0].pBuffer    = pSound->pData;
	pPcmParams->buffs[0].nNumBytes  = pSound->nNumBytes;
	pPcmParams->buffs[0].nLoopCount = loopCount;
	pPcmParams->buffs[0].nNextBuff  = SCE_NGS_PLAYER_NO_NEXT_BUFFER;

	// Update player parameters
	returnCode = sceNgsVoiceUnlockParams( hVoice, nModuleId );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "initPlayer: sceNgsVoiceUnlockParams() failed: 0x%08X\n", returnCode );
		if ( returnCode == SCE_NGS_ERROR_PARAM_OUT_OF_RANGE ) {
			printParamError( hVoice, nModuleId );
		}
		return returnCode;
	}

	return SCE_OK;
}

int SoundProcessor::connectRacks( SceNgsHVoice hVoiceSource, SceNgsHVoice hVoiceDest )
{
	int                  returnCode;
	SceNgsPatchSetupInfo patchInfo;
	SceNgsHPatch         patch;
	SceNgsPatchRouteInfo patchRouteInfo;

	// Create patch
	patchInfo.hVoiceSource           = hVoiceSource;
	patchInfo.nSourceOutputIndex     = 0;
	patchInfo.nSourceOutputSubIndex  = SCE_NGS_VOICE_PATCH_AUTO_SUBINDEX;
	patchInfo.hVoiceDestination      = hVoiceDest;
	patchInfo.nTargetInputIndex      = 0;

	returnCode = sceNgsPatchCreateRouting( &patchInfo, &patch );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "connectRacks: sceNgsPatchCreateRouting() failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	// Set volumes on patch
	returnCode = sceNgsPatchGetInfo( patch, &patchRouteInfo, NULL );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "connectRacks: sceNgsPatchGetInfo() failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	if ( patchRouteInfo.nOutputChannels == 1 ) {
		if ( patchRouteInfo.nInputChannels == 1 ) {
			patchRouteInfo.vols.m[0][0] = 1.0f; // left to left
		} else {
			patchRouteInfo.vols.m[0][0] = 1.0f; // left to left
			patchRouteInfo.vols.m[0][1] = 1.0f; // left to right
		}
	} else {
		if ( patchRouteInfo.nInputChannels == 1 ) {
			patchRouteInfo.vols.m[0][0] = 1.0f; // left to left
			patchRouteInfo.vols.m[1][0] = 1.0f; // right to left
		} else {
			patchRouteInfo.vols.m[0][0] = 1.0f; // left to left
			patchRouteInfo.vols.m[0][1] = 0.0f; // left to right
			patchRouteInfo.vols.m[1][0] = 0.0f; // right to left
			patchRouteInfo.vols.m[1][1] = 1.0f; // right to right
		}
	}
	returnCode = sceNgsVoicePatchSetVolumesMatrix( patch, &patchRouteInfo.vols );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "connectRacks: sceNgsVoicePatchSetVolumesMatrix() failed: 0x%08X\n", returnCode );
		return returnCode;
	}

	return SCE_OK;
}

int SoundProcessor::prepareAudioOut( int nMode, int nBufferGranularity, int nSampleRate, const char *strFileName )
{
	if ( nMode & NGS_SEND_TO_DEVICE ) {
		s_nAudioOutPortId = sceAudioOutOpenPort(	SCE_AUDIO_OUT_PORT_TYPE_MAIN,
													nBufferGranularity,						//grain size
													nSampleRate,							//output frequency
													SCE_AUDIO_OUT_PARAM_FORMAT_S16_STEREO);	//format

		if ( s_nAudioOutPortId < 0 ) {
			printf( "prepareAudioOut: sceAudioOutOpenPort() failed: 0x%08X\n", s_nAudioOutPortId );
			return s_nAudioOutPortId;
		}

		int volume[2] = { SCE_AUDIO_VOLUME_0dB, SCE_AUDIO_VOLUME_0dB };
		sceAudioOutSetVolume( s_nAudioOutPortId, (SCE_AUDIO_VOLUME_FLAG_L_CH | SCE_AUDIO_VOLUME_FLAG_R_CH), volume );

		printf( "prepareAudioOut: sending output to audio port %d\n", s_nAudioOutPortId );
	}

	// Open file for output
	if ( nMode & NGS_WRITE_TO_FILE ) {
		if ( !strFileName ) {
			nMode = nMode & NGS_SEND_TO_DEVICE;
		} else {
			s_uidAudioOutFile = sceIoOpen( strFileName, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, SCE_STM_RWU );
			if ( s_uidAudioOutFile < 0 ) {
				printf( "prepareAudioOut: sceIoOpen() failed: 0x%08X\n", s_uidAudioOutFile );
				return s_uidAudioOutFile;
			}
			printf( "prepareAudioOut: writing output to \"%s\"\n", strFileName );
		}
	}
	s_nAudioOutMode      = nMode;
	s_uNumBytesPerUpdate = sizeof(short) * nBufferGranularity * 2;

	return SCE_OK;
}

int SoundProcessor::startAudioUpdateThread( void )
{
	int returnCode;

	s_threadIdAudioUpdate = sceKernelCreateThread(	"Audio Update Thread", _AudioUpdateThread,
													SCE_KERNEL_HIGHEST_PRIORITY_USER,
													AUDIO_UPDATE_THREAD_STACK_SIZE, 0,
													SCE_KERNEL_CPU_MASK_USER_2, SCE_NULL );
	if ( s_threadIdAudioUpdate < 0 ) {
		printf( "startAudioUpdateThread: sceKernelCreateThread() failed: %08x\n", s_threadIdAudioUpdate );
		return s_threadIdAudioUpdate;
	}
	returnCode = sceKernelStartThread( s_threadIdAudioUpdate, 0, NULL );
	if ( returnCode < 0 ) {
		printf( "startAudioUpdateThread: sceKernelStartThread() failed: %08x\n", returnCode );
		return returnCode;
	}
	printf( "startAudioUpdateThread: started AudioUpdateThread\n" );


	return SCE_OK;
}

int SoundProcessor::process(void)
{
	int nLocalFrameCount = 0;
	int returnCode       = SCE_OK;

	while ( (volatile int)s_nTickCount < NUM_FRAMES ) {

		if ( nLocalFrameCount < s_nTickCount ) {
			nLocalFrameCount = s_nTickCount;

			// Print simple console message every 30 updates
			if ( nLocalFrameCount % 30 == 0 ) {
				char msg[64];
				snprintf( msg, sizeof(msg), "Frame %d\n", nLocalFrameCount );
				printf( msg );
			}
		}

		sceKernelDelayThread( SLEEP_PERIOD );
	}

	return returnCode;
}

int SoundProcessor::shutdown(void)
{
	int      returnCode = SCE_OK;
	SceInt32 n32ThreadExitStatus;


	// Stop audio update thread
	s_bRun = false;
	returnCode = sceKernelWaitThreadEnd( s_threadIdAudioUpdate, &n32ThreadExitStatus, SCE_NULL );
	if ( returnCode != SCE_OK ) {
		printf( "shutdown: sceKernelWaitThreadEnd() failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	printf( "shutdown: _AudioUpdateThread exit'd (exit status = %d)\n", n32ThreadExitStatus );


	// Close audio out port and/or file
	shutdownAudioOut();


	// Shutdown NGS
	returnCode = sceNgsSystemRelease( s_sysHandle );
	if ( returnCode != SCE_NGS_OK ) {
		printf( "shutdown: sceNgsSystemRelease() failed: 0x%08X\n", returnCode );
		return returnCode;
	}
	sceSysmoduleUnloadModule( SCE_SYSMODULE_NGS );


	free( s_pRackMemPlayerMusic );
	free( s_pRackMemPlayerSFX );
	free( s_pRackMemMaster );
	free( s_pSysMem );
	free( s_music.pData );
	free( s_sfx.pData );

	return returnCode;
}

void SoundProcessor::shutdownAudioOut( void )
{
	int returnCode;

	if ( s_nAudioOutMode & NGS_SEND_TO_DEVICE ) {
		// Wait for output of all registered audio data
		returnCode = sceAudioOutOutput( s_nAudioOutPortId, NULL );
		if ( returnCode < 0 ) {
			printf( "shutdownAudioOut: sceAudioOutOutput(port %d, NULL) failed: 0x%08X\n", s_nAudioOutPortId, returnCode );
		}

		returnCode = sceAudioOutReleasePort( s_nAudioOutPortId );
		if ( returnCode < 0 ) {
			printf( "shutdownAudioOut: sceAudioOutReleasePort() failed: 0x%08X\n", returnCode );
		}
	}

	if ( s_nAudioOutMode & NGS_WRITE_TO_FILE ) {
		returnCode = sceIoClose( s_uidAudioOutFile );
		if ( returnCode < 0 ) {
			printf( "shutdownAudioOut: sceIoClose() failed: 0x%08X\n", returnCode );
		}
	}
}

int SoundProcessor::loadWAVFile( const char *strFileName, SoundInfo *pSound )
{
	int       returnCode = SCE_OK;
	SceUID    uid;
	char      pReadBuffer[8];
	SceUInt32 uChunkSize;
	SceSize   szBytesLeft;
	char      *pCurrentPos;

	uid = sceIoOpen( strFileName, SCE_O_RDONLY, 0 );
	if ( uid < 0 ) {
		printf( "loadWAVFile: sceIoOpen() failed: 0x%08X\n", uid );
		return uid;
	}

	/* Check file is a RIFF file */
	returnCode = sceIoRead( uid, pReadBuffer, 4 );
	if ( returnCode != 4 ) {
		printf( "loadWAVFile: sceIoRead() failed: 0x%08X\n", returnCode );
		sceIoClose( uid );
		return ( ( returnCode < 0 ) ? returnCode : SCE_ERROR_ERRNO_ENOMEM );
	} else if ( strncmp( pReadBuffer, "RIFF", 4 ) != 0 ) {
		printf( "loadWAVFile: invalid file format - not RIFF\n" );
		sceIoClose( uid );
		return -1;
	}

	/* Check file is a WAV file */
	returnCode = sceIoRead( uid, pReadBuffer, 8 );
	if ( returnCode != 8 ) {
		printf( "loadWAVFile: sceIoRead() failed: 0x%08X\n", returnCode );
		sceIoClose( uid );
		return -1;
	} else if ( strncmp( &pReadBuffer[4], "WAVE", 4 ) != 0 ) {
		printf( "loadWAVFile: invalid file format - not WAVE\n" );
		sceIoClose( uid );
		return -1;
	}

	/* Scan file for "fmt " chunk */
	returnCode = scanRIFFFileForChunk( "fmt ", uid, &uChunkSize );
	if ( returnCode != SCE_OK ) {
		printf( "loadWAVFile: invalid file format - no fmt chunk\n" );
		sceIoClose( uid );
		return -1;
	}

	/* Found "fmt " chunk, read 8 bytes (#channels is read+2, sample rate is read+4) */
	returnCode = sceIoRead( uid, pReadBuffer, 8 );
	if ( returnCode != 8 ) {
		printf( "loadWAVFile: sceIoRead() failed: 0x%08X\n", returnCode );
		sceIoClose( uid );
		return -1;
	}

	pSound->nNumChannels = *( (SceInt16 *)(void *)(pReadBuffer + 2) );
	if ( (pSound->nNumChannels != 0x01) && (pSound->nNumChannels != 0x02) ) {
		printf( "loadWAVFile: invalid file format - number of channels\n" );
		sceIoClose( uid );
		return -1;
	}
	pSound->nSampleRate = *( (SceUInt32 *)(void *)(pReadBuffer + 4) );


	/* Return to start of chunks, and scan for "data" chunk */
	returnCode = sceIoLseek( uid, 12, SCE_SEEK_SET );
	if ( returnCode < 0 ) {
		printf( "loadWAVFile: sceIoLseek() failed: 0x%08X\n", returnCode );
		sceIoClose( uid );
		return -1;
	}
	returnCode = scanRIFFFileForChunk( "data", uid, &uChunkSize );
	if ( returnCode != SCE_OK ) {
		printf( "loadWAVFile: invalid file format - no data chunk\n" );
		sceIoClose( uid );
		return -1;
	}

	/* Found "data" chunk, allocate memory and read PCM data */
	pSound->nNumBytes = uChunkSize;
	pSound->pData = malloc( pSound->nNumBytes );
	if ( pSound->pData == NULL ) {
		printf( "loadWAVFile: malloc() failed for %d bytes\n", pSound->nNumBytes );
		sceIoClose( uid );
		return SCE_ERROR_ERRNO_ENOMEM;
	}

	/* Read file */
	pCurrentPos = (char *)pSound->pData;
	szBytesLeft = uChunkSize;
	while ( szBytesLeft > FILE_READ_CHUNK_SIZE ) {
		returnCode = sceIoRead( uid, pCurrentPos, FILE_READ_CHUNK_SIZE );
		if ( returnCode != FILE_READ_CHUNK_SIZE ) {
			printf( "loadWAVFile: sceIoRead(data chunk) failed: 0x%08X\n", returnCode );
			sceIoClose( uid );
			return ( ( returnCode < 0 ) ? returnCode : SCE_ERROR_ERRNO_ENOMEM );
		}
		pCurrentPos += FILE_READ_CHUNK_SIZE;
		szBytesLeft -= FILE_READ_CHUNK_SIZE;
	}
	if ( szBytesLeft > 0 ) {
		returnCode = sceIoRead( uid, pCurrentPos, szBytesLeft );
		if ( returnCode != szBytesLeft ) {
			printf( "loadWAVFile: sceIoRead(data chunk-end) failed: 0x%08X\n", returnCode );
			sceIoClose( uid );
			return ( ( returnCode < 0 ) ? returnCode : SCE_ERROR_ERRNO_ENOMEM );
		}
	}
	sceIoClose(uid);

	pSound->nType = SCE_NGS_PLAYER_TYPE_PCM;

	return SCE_OK;
}

int SoundProcessor::scanRIFFFileForChunk( const char *strChunkId, SceUID uid, SceUInt32 *puChunkSize )
{
	int       returnCode;
	bool      bFoundChunk = false;
	union {
		char      c[4];
		SceUInt32 ui;
	} readBuffer;


	*puChunkSize = 0;
	while ( !bFoundChunk ) {
		returnCode = sceIoLseek( uid, *puChunkSize, SEEK_CUR );
		if ( returnCode < 0 ) {
			printf( "scanRIFFFileForChunk: sceIoLseek() failed: 0x%08X\n", returnCode );
			sceIoClose( uid );
			return -1;
		}
		returnCode = sceIoRead( uid, readBuffer.c, 4 );
		if ( returnCode != 4 ) {
			printf( "scanRIFFFileForChunk: sceIoRead() failed: 0x%08X\n", returnCode );
			sceIoClose( uid );
			return -1;
		}

		bFoundChunk = ( strncmp( readBuffer.c, strChunkId, 4 ) == 0 );

		returnCode = sceIoRead( uid, readBuffer.c, 4 );
		if ( returnCode != 4 ) {
			printf( "scanRIFFFileForChunk: sceIoRead() failed: 0x%08X\n", returnCode );
			sceIoClose( uid );
			return -1;
		}
		*puChunkSize = readBuffer.ui;
	}

	return SCE_OK;
}

void SoundProcessor::printParamError( SceNgsHVoice hVoice, SceUInt32 nModuleId )
{
	char str[128];

	sceNgsVoiceGetParamsOutOfRange( hVoice, nModuleId, str );
	printf( "PARAM ERROR is \"%s\"\n", str );
}

int SoundProcessor::_AudioUpdateThread( SceSize args, void *argc )
{
	int   returnCode     = SCE_OK;
	int   nCurrentBuffer = 0;
	short outputData[2][ SYS_GRANULARITY * 2 ];

	printf( "_AudioUpdateThread running\n" );

	while ( (volatile bool) s_bRun ) {
		++s_nTickCount;

		// Perform NGS system update
		returnCode = sceNgsSystemUpdate( s_sysHandle );
		if ( returnCode != SCE_NGS_OK ) {
			printf( "_AudioUpdateThread: sceNgsSystemUpdate() failed: 0x%08X\n", returnCode );
			break;
		}

		// Get output data
		returnCode = sceNgsVoiceGetStateData(	s_voiceMaster, SCE_NGS_MASTER_BUSS_OUTPUT_MODULE,
												outputData[nCurrentBuffer], sizeof(short) * SYS_GRANULARITY * 2 );
		if ( returnCode != SCE_NGS_OK ) {
			printf( "_AudioUpdateThread: sceNgsVoiceGetStateData() failed: 0x%08X\n", returnCode );
			break;
		}

		// Output audio to device and/or file
		returnCode = writeAudioOut( outputData[nCurrentBuffer] );

		// Switch buffer
		nCurrentBuffer ^= 1;
	}

	printf( "_AudioUpdateThread exiting\n" );
	sceKernelExitDeleteThread( returnCode );

	return returnCode;
}

int SoundProcessor::writeAudioOut( const short *pBuffer )
{
	int returnCode;

	// Send audio to output port
	if ( s_nAudioOutMode & NGS_SEND_TO_DEVICE ) {
		returnCode = sceAudioOutOutput( s_nAudioOutPortId, pBuffer );
		if ( returnCode < 0 ) {
			printf( "writeAudioOut: sceAudioOutOutput() failed: 0x%08X\n", returnCode );
			return returnCode;
		}
	}

	// Write audio to file
	if ( s_nAudioOutMode & NGS_WRITE_TO_FILE ) {
		returnCode = sceIoWrite( s_uidAudioOutFile, pBuffer, s_uNumBytesPerUpdate );
		if ( returnCode != s_uNumBytesPerUpdate ) {
			printf( "writeAudioOut: sceIoWrite() failed: 0x%08X\n", returnCode );
			return returnCode;
		}
	}

	return SCE_OK;
}